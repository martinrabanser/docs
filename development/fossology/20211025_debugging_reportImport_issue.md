---
title: Debugging reportImport agent issue
author: Alberto Pianon <pianon@array.eu>
date: "2021-10-25"
lastmod: "2021-10-25"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# Debugging reportImport agent issue

The issue is described in [20210819_report_on_fossology_integration.md](./20210819_report_on_fossology_integration.md), Section 2

- [x] find how to reproduce the problem
- [x] ~~debug php code~~ it's not a php bug!
- [x] define a strategy with the Audit Team to delete wrong imported data and re-import the same data in the correct way, allowing the Audit Team to continue working during delete/reimport operations

----------------

reportImport findings are applied to files in the file repository (table "pfile"), not to uploads (table "uploadtree_a"). This means that if a file is found in more than one upload, the findings will be applied to all uploads containing that file.

Even if you find the error in a file in an upload, that error may be due to a reportImport error on _another_ upload, that shares the same file

In the `reportimport_ars` db table, you get the exact date and time of every reportImport job, together with the upload id. So if you match the reportImport job datetime on `reportimport_ars` table with the finding timestamp in `license_file` table, you can reconstruct which reportImport job on which upload caused the problem


let's start with this file, found in a Package Python 3.8.2

![before](img/debug_reportImport01.png)

Upload ID is 368 and upload file ID in `uploadtree_a` is 1478452.

```sql
select pfile_fk from uploadtree_a where uploadtree_pk = 1478452;
```

returns 53080.

```sql
select * from license_file where pfile_fk = 53080 and agent_fk = 26;
```

returns a long list of findings with many different datestamps

(26 is the agent id for reportImport in the instance used for this test)

```sql
select lf.pfile_fk, array_agg(lf.rf_timestamp), rf.rf_shortname, r.upload_fk, u.upload_filename
from license_file lf, license_ref rf, reportimport_ars r, upload u
where
  lf.pfile_fk = 53080
  and lf.agent_fk = 26
  and lf.rf_timestamp  >= r.ars_starttime
  and lf.rf_timestamp  <= r.ars_endtime
  and rf.rf_pk = lf.rf_fk
  and u.upload_pk = r.upload_fk
group by lf.pfile_fk, rf.rf_shortname, r.upload_fk, u.upload_filename
order by r.upload_fk
```

returns the list of reportImport findings with aggregated timestamp, license finding and upload id and name

results are here [dump.csv](data/dump.csv)

let's restrict the search to look only for recent strange results:

```sql
select lf.pfile_fk, array_agg(lf.rf_timestamp), rf.rf_shortname, r.upload_fk, u.upload_filename
from license_file lf, license_ref rf, reportimport_ars r, upload u
where
  lf.pfile_fk = 53080
  and lf.agent_fk = 26
  and lf.rf_timestamp  >= r.ars_starttime
  and lf.rf_timestamp  <= r.ars_endtime
  and rf.rf_pk = lf.rf_fk
  and u.upload_pk = r.upload_fk
  and rf.rf_shortname not in ('X11', 'scancode-public-domain')
  and u.upload_filename like '%@%'
group by lf.pfile_fk, rf.rf_shortname, r.upload_fk, u.upload_filename
order by r.upload_fk
```

we get:

<table>
<tbody><tr><th class="data">pfile_fk</th><th class="data">array_agg</th><th class="data">rf_shortname</th><th class="data">upload_fk</th><th class="data">upload_filename</th></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 09:51:50.279912+00","2021-04-05 09:51:50.280604+00"}</td><td style="white-space:nowrap;">curl</td><td style="white-space:nowrap;"><div style="text-align: right">302</div></td><td style="white-space:nowrap;">curl@7.69.1-r0-5a80bbfd</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 14:24:51.950293+00","2021-04-05 14:24:51.951354+00"}</td><td style="white-space:nowrap;">GPL-3.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">339</div></td><td style="white-space:nowrap;">libidn2@2.3.0-r0-7ef87e61</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 16:19:45.788341+00","2021-04-05 16:19:45.789035+00"}</td><td style="white-space:nowrap;">GPL-2.0-only</td><td style="white-space:nowrap;"><div style="text-align: right">347</div></td><td style="white-space:nowrap;">alsa-utils@1.2.1-r0-ad72568e</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 16:32:10.074816+00","2021-04-05 16:32:10.075684+00"}</td><td style="white-space:nowrap;">GPL-2.0-only</td><td style="white-space:nowrap;"><div style="text-align: right">352</div></td><td style="white-space:nowrap;">logrotate@3.15.1-r0-a6faf6ab</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 17:22:52.463957+00","2021-04-05 17:22:52.46465+00"}</td><td style="white-space:nowrap;">LGPL-2.1-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">363</div></td><td style="white-space:nowrap;">fribidi@1.0.9-r0-828407f1</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 18:55:00.511837+00","2021-04-05 18:55:00.512615+00"}</td><td style="white-space:nowrap;">GPL-2.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">388</div></td><td style="white-space:nowrap;">util-linux@2.35.1-r0-daca375c</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-05 20:08:28.126425+00","2021-04-05 20:08:28.127309+00"}</td><td style="white-space:nowrap;">MIT-1</td><td style="white-space:nowrap;"><div style="text-align: right">406</div></td><td style="white-space:nowrap;">libxml2@2.9.10-r0-7deadce9</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 00:20:29.200478+00","2021-04-06 00:20:29.201199+00"}</td><td style="white-space:nowrap;">LPGL-2.1-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">435</div></td><td style="white-space:nowrap;">alsa-lib@1.2.1.2-r0-6e6c86f4</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 00:50:37.37147+00","2021-04-06 00:50:37.372246+00"}</td><td style="white-space:nowrap;">ISC</td><td style="white-space:nowrap;"><div style="text-align: right">450</div></td><td style="white-space:nowrap;">cronie@1.5.5-r0-0c105cd2</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 00:56:12.24377+00","2021-04-06 00:56:12.244624+00"}</td><td style="white-space:nowrap;">GPL-3.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">452</div></td><td style="white-space:nowrap;">gzip@1.10-r0-2a5a7763</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 01:18:15.867861+00","2021-04-06 01:18:15.868745+00"}</td><td style="white-space:nowrap;">LGPL-2.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">453</div></td><td style="white-space:nowrap;">gtk+3@3.24.14-r0-4c49a9aa</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 03:10:46.52817+00","2021-04-06 03:10:46.528863+00"}</td><td style="white-space:nowrap;">MIT</td><td style="white-space:nowrap;"><div style="text-align: right">480</div></td><td style="white-space:nowrap;">harfbuzz@2.6.4-r0-ee6269a7</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 03:16:48.186978+00","2021-04-06 03:16:48.188342+00"}</td><td style="white-space:nowrap;">AFL-2.1</td><td style="white-space:nowrap;"><div style="text-align: right">483</div></td><td style="white-space:nowrap;">dbus@1.12.16-r0-38a0da79</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 03:16:48.187684+00"}</td><td style="white-space:nowrap;">Dual-license</td><td style="white-space:nowrap;"><div style="text-align: right">483</div></td><td style="white-space:nowrap;">dbus@1.12.16-r0-38a0da79</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 03:16:48.186238+00","2021-04-06 03:16:48.189004+00"}</td><td style="white-space:nowrap;">GPL-2.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">483</div></td><td style="white-space:nowrap;">dbus@1.12.16-r0-38a0da79</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-04-06 03:21:28.070422+00","2021-04-06 03:21:28.071313+00","2021-04-06 03:23:43.589763+00","2021-04-06 03:23:43.590669+00"}</td><td style="white-space:nowrap;">GPL-3.0-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">484</div></td><td style="white-space:nowrap;">gawk@5.0.1-r0-2be6e0f2</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-07-11 23:14:15.748597+00"}</td><td style="white-space:nowrap;">LPGL-2.1-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">544</div></td><td style="white-space:nowrap;">alsa-lib@1.2.1.2-r0-c619629c</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-07-11 23:50:16.870806+00"}</td><td style="white-space:nowrap;">curl</td><td style="white-space:nowrap;"><div style="text-align: right">553</div></td><td style="white-space:nowrap;">curl@7.69.1-r0-5d67b352</td></tr>
<tr class="data1">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-07-13 04:32:48.377312+00"}</td><td style="white-space:nowrap;">LPGL-2.1-or-later</td><td style="white-space:nowrap;"><div style="text-align: right">593</div></td><td style="white-space:nowrap;">alsa-lib@1.2.1.2-r0-c619629c</td></tr>
<tr class="data2">
<td style="white-space:nowrap;"><div style="text-align: right">53080</div></td><td style="white-space:nowrap;">{"2021-07-13 06:13:08.010017+00"}</td><td style="white-space:nowrap;">curl</td><td style="white-space:nowrap;"><div style="text-align: right">604</div></td><td style="white-space:nowrap;">curl@7.69.1-r0-5d67b352</td></tr>
</tbody></table>

let's look into the imported spdx report fo curl@7.69.1-r0-5d67b352

[1617616308_576308756_curl-7.69.1.tar.bz2.alien.spdx.rdf](data/1617616308_576308756_curl-7.69.1.tar.bz2.alien.spdx.rdf)

```xml
            <spdx:hasFile>
              <spdx:File rdf:about="http://spdx.org/spdxdocs/curl-7.69.1-r0-07b2db3f-f215-480a-b0f2-53748480beb0#SPDXRef-file-1f8fb5aabca99ab0d27adde4f139c18d">
                <spdx:checksum>
                  <spdx:Checksum>
                    <spdx:checksumValue>45053d3e25fb26a5b3c4a00a23d6b572086a2445</spdx:checksumValue>
                    <spdx:algorithm rdf:resource="http://spdx.org/rdf/terms#checksumAlgorithm_sha1"/>
                  </spdx:Checksum>
                </spdx:checksum>
                <spdx:copyrightText rdf:resource="http://spdx.org/rdf/terms#none"/>
                <spdx:licenseInfoInFile rdf:resource="http://spdx.org/licenses/curl"/>
                <spdx:licenseConcluded rdf:resource="http://spdx.org/licenses/curl"/>
                <spdx:fileName>curl@7.69.1-r0/curl-7.69.1.tar.bz2/curl-7.69.1.tar/curl-7.69.1/install-sh</spdx:fileName>
              </spdx:File>
            </spdx:hasFile>
```

what happened here? The file name (`install-sh`) and the checksum are the same as the ones found in Python 3.8.2 upload.

But here we have `curl` both as `licenseInfoInFile` and `licenseConcluded` even if there is no reference to `curl` license in the source file. Why?

Because the imported SPDX record is generated from the Debian copyright file which has this `Files` entry:

```yml
Files: *
Copyright: 1996-2015, Daniel Stenberg <daniel@haxx.se>
License: curl
```

After that, there are other `Files` entries with other licenses, but none of them covers `install-sh`, so the latter takes the `curl` license from the wildcard entry above.

aliens4friends' spdxdebian and spdxalien commands import findings from Debian copyright both as scanner findings (`licenseInfoInFile`) and as decisions (`licenseConcluded`) on all files containing at least some copyright/license notice. The problem is that decisions are specific to one single upload (and group), while findings are applied to all occurrencies of the same file in other uploads.

Therefore, aliens4friends must be modified in order to import Debian copyright entries not as `licenseInfoInFile` but just as decisions (`licenseConcluded`).

Moreover, it should be discussed with the Audit Team how to import Debian findings correctly

just to double-check, this is the imported spdx report for dbus@1.12.16-r0-38a0da79

[1617679004_686409455_dbus-1.12.16.tar.gz.alien.spdx.rdf](data/1617679004_686409455_dbus-1.12.16.tar.gz.alien.spdx.rdf)

```xml
            <spdx:hasFile>
              <spdx:File rdf:about="http://spdx.org/spdxdocs/dbus-1.12.16-r0-8999bd2e-0d26-4466-ab84-09a2fcaebacf#SPDXRef-file-bc0b822039008b59b53d8543373614bf">
                <spdx:checksum>
                  <spdx:Checksum>
                    <spdx:checksumValue>45053d3e25fb26a5b3c4a00a23d6b572086a2445</spdx:checksumValue>
                    <spdx:algorithm rdf:resource="http://spdx.org/rdf/terms#checksumAlgorithm_sha1"/>
                  </spdx:Checksum>
                </spdx:checksum>
                <spdx:copyrightText rdf:resource="http://spdx.org/rdf/terms#none"/>
                <spdx:licenseInfoInFile rdf:resource="http://spdx.org/licenses/GPL-2.0-or-later"/>
                <spdx:licenseInfoInFile rdf:resource="http://spdx.org/licenses/AFL-2.1"/>
                <spdx:licenseConcluded>
                  <spdx:DisjunctiveLicenseSet>
                    <spdx:member rdf:resource="http://spdx.org/licenses/AFL-2.1"/>
                    <spdx:member rdf:resource="http://spdx.org/licenses/GPL-2.0-or-later"/>
                  </spdx:DisjunctiveLicenseSet>
                </spdx:licenseConcluded>
                <spdx:fileName>dbus@1.12.16-r0/dbus-1.12.16.tar.gz/dbus-1.12.16.tar/dbus-1.12.16/build-aux/install-sh</spdx:fileName>
              </spdx:File>
            </spdx:hasFile>
```

and this is the relevant entry in the Debian copyright file used to generate the above xml:

```yaml
Files: *
Copyright:
 © 1994 A.M. Kuchling
 © 2002-2008 Red Hat, Inc
 © 2002-2003 CodeFactory AB
 © 2002 Michael Meeks
 © 2004 Imendio HB
 © 2005 Lennart Poettering
 © 2005 Novell, Inc
 © 2005 David A. Wheeler
 © 2006-2013 Ralf Habacker
 © 2006 Mandriva
 © 2006 Peter Kümmel
 © 2006 Christian Ehrlicher
 © 2006 Thiago Macieira
 © 2008 Colin Walters
 © 2009 Klaralvdalens Datakonsult AB, a KDAB Group company
 © 2011-2012 Nokia Corporation
 © 2012-2018 Collabora Ltd.
 © 2013 Intel Corporation
 © 2017 Laurent Bigonville
 © 2018 KPIT Technologies Ltd.
 © 2018 Manish Narang
 "modified code from libassuan, (C) FSF"
License: GPL-2+ or AFL-2.1
```
