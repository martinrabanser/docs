---
title: SQL queries to fix import issue - activity log
author: Alberto Pianon <pianon@array.eu>
date: "2021-10-26"
lastmod: "2022-05-11"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# SQL queries to fix import issue - activity log

The issue is described in [20210819_report_on_fossology_integration.md](./20210819_report_on_fossology_integration.md), Section 2, while the steps taken to identify the cause and reproduce it are described in [20211025_debugging_reportImport_issue.md](./20211025_debugging_reportImport_issue.md)


- [x] find the correct set of sql queries to delete all reportImport findings and to be able to re-run the agent
- [x] get lists of uploads to be processed (id and name)
- uploads in deb folder:
  - [x] apply the queries and verify the result
  - [x] check if it's necessary to recover something from the "old" pool in the old testing VM
  - [x] create an a4f session file with all the required packages
  - [x] regenerate all `.debian.spdx` and `.alien.spdx` files in the pool ()
  - [x] schedule all reportImport jobs by creating a dedicated a4f session (it may be necessary to recover some old package variants from old pool)
  - [x] check the results with @rahulmohang
- [x] uploads in inbox and inbox-priority folder
  - same steps as above
- [x] uploads in the Goofy folder
  - same as above

--------------------------------------------------------------------------
*Date: Oct 26, 2021*

complete set of sql queries (for each upload)

```sql
-- 1 delete clearing decisions
DELETE FROM clearing_decision cd WHERE cd.clearing_decision_pk IN
(
   SELECT DISTINCT cde.clearing_decision_fk
   FROM clearing_decision_event cde
   WHERE cde.clearing_event_fk IN
   (
       SELECT DISTINCT ce.clearing_event_pk
       FROM clearing_event ce, uploadtree_a ut
       WHERE
           ut.uploadtree_pk = ce.uploadtree_fk
           AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
           AND ut.upload_fk IN {upload_ids}
   )
);

-- 2 delete entries in clearing decision event table

DELETE FROM clearing_decision_event cde
WHERE cde.clearing_event_fk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN {upload_ids}
);

-- 3 delete entries in clearing event table

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN {upload_ids}
);

-- 4 delete license file entries

DELETE FROM license_file lf1 WHERE lf1.fl_pk IN
(
 SELECT DISTINCT lf.fl_pk
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN {upload_ids}
   AND lf.agent_fk = a.agent_pk
   AND a.agent_name = 'reportImport'
);


-- 5 delete reportimport_ars records for upload

DELETE FROM reportimport_ars WHERE upload_fk IN {upload_ids};
```

collect lists of uploads to process

```python
from aliens4friends.commons.fossywrapper import FossyWrapper
fw = FossyWrapper()
uploads = fw.fossology.list_uploads()
uploads2process = {}
uploads2process["working/deb"] = { u.uploadname: u.id for u in uploads if u.foldername == "deb" }
uploads2process["working/deb"].pop("curl@7.69.1-r0-5a80bbfd") # already processed
uploads2process["inbox"] = { u.uploadname: u.id for u in uploads if u.foldername == "inbox" }
uploads2process["inbox-priority"] = { u.uploadname: u.id for u in uploads if u.foldername == "inbox-priority" }

```

lists:

```python
uploads2process["working/deb"]

{'npth@1.6-r0-d92f3809': 292,
 'weston@8.0.0-r0-21b005d3': 296,
 'libaio@0.3.111-r0-78cc3218': 301,
 'wpa-supplicant@2.9-r0-6cf418a6': 309,
 'dbus-glib@0.110-r0-0520936c': 310,
 'gmmlib@20.4.1-r0-e9f000fe': 311,
 'ncurses@6.2-r0-d400ac85': 314,
 'systemd-boot@244.5-r0-d58a7447': 315,
 'json-c@0.13.1-r0-98c91553': 320,
 'libarchive@3.4.2-r0-3faee6c5': 326,
 'bind@9.11.22-r0-0f274724': 327,
 'librepo@1.11.2-r0-0470dea1': 329,
 'gdb@9.1-r0-f8ae60ef': 330,
 'thermald@2.2-r0-7bc12963': 334,
 'libidn2@2.3.0-r0-7ef87e61': 339,
 'pango@1.44.7-r0-7a541eb6': 340,
 'libepoxy@1.5.4-r0-91c188a2': 341,
 'zlib-intel@1.2.11.1.jtkv6.3-r0-528c7d5f': 343,
 'iperf3@3.7-r0-81ffd3b6': 344,
 'json-glib@1.4.4-r0-eae22de5': 350,
 'logrotate@3.15.1-r0-a6faf6ab': 352,
 'systemd@244.5-r0-73c6e7b8': 353,
 'libcap@2.32-r0-b251c36e': 358,
 'perl@5.30.1-r0-8dffff84': 364,
 'libcomps@0.1.15-r0-11cd46e8': 371,
 'zlib@1.2.11-r0-1eea2d14': 378,
 'sqlite3@3.31.1-r0-ce380b0f': 380,
 'clutter-1.0@1.26.2-r0-aa3158f5': 383,
 'procps@3.3.16-r0-7a7da76a': 384,
 'iw@5.4-r0-07a1f344': 389,
 'dropbear@2019.78-r0-58f1e2b7': 393,
 'intel-media-driver@20.4.5-r0-a799b8d6': 394,
 'freetype@2.10.1-r0-356852b2': 397,
 'dhcp@4.4.2-r0-ea0e5da9': 401,
 'hdparm@9.58-r0-0130acc3': 470}
```

```python
uploads2process["inbox"]

{'xserver-xf86-config@0.1-r33-396dd9a5': 562,
 'wpa-supplicant@2.9-r0-3743f1ac': 563,
 'bluez-firmware-rpidistro@0.0+gitAUTOINC+96eefffccc-r0-f7d01616': 565,
 'linux-firmware-bluetooth-bcm43455@1.0-r0-01197f0f': 566,
 'u-boot-stm32mp-extlinux@3.1-r0-73c2567a': 567,
 'libpcre@8.44-r0-5135e3b4': 568,
 'run-postinsts@1.0-r10-3db4063d': 569,
 'openssl@1.1.1k-r0-a513c6ec': 570,
 'udev-rules-rpi@1.0-r0-67bcdba9': 571,
 'linux-firmware-rpidistro@20190114-1+rpt8-r0-f5df305c': 572,
 'python3@3.8.2-r0-783eccc4': 574,
 'linux-seco@5.4.70+gitAUTOINC+9982915529-r0-7a6c3ac7': 575,
 'bind@9.11.22-r0-96ae9920': 576,
 'm4projects-stm32mp1@1.4.0-r0-db24a44c': 577,
 'zephyr-philosophers@2.6.0+gitAUTOINC+79a6c07536_c3bd2094f9-r0-fdcfacd5': 578,
 'linux-stm32mp@5.10.10-r0-efbaea0a': 579,
 'busybox@1.31.1-r0-ac163238': 584,
 'libevdev@1.8.0-r0-bb9b3265': 585,
 'binutils@2.36.1-r0-5c76fe39': 586,
 'apmd@3.2.2-15-r0-ebd67536': 587,
 'optee-client@3.7.0+gitAUTOINC+bc0ec8ce1e-r0-58c306be': 588,
 'ohos-global-i18n-lite@0.0+gitAUTOINC+1de9116474-r0-65b36e1b': 589,
 'librepo@1.11.2-r0-02dc9319': 590,
 'alsa-lib@1.2.1.2-r0-c619629c': 593,
 'm4fwcoredump@1.0-r0-2b0661fe': 595,
 'u-boot-stm32mp-splash@2018.11-r0-b4c90484': 596,
 'linux-raspberrypi@5.4.72+gitAUTOINC+5d52d9eea9_154de7bbd5-r0-e1dc9897': 598,
 'linux-firmware@20210208-r0-c17ff54c': 599,
 'ohos-xts-acts@0.0+gitAUTOINC+defe7649c4-r0-f875f462': 600,
 'pi-bluetooth@0.1.12-r0-77dec538': 601,
 'alsa-state-stm32mp1@1.0-r0-963ab037': 602,
 'dropbear@2020.81-r0-175a462f': 603,
 'curl@7.69.1-r0-5d67b352': 604,
 'linux-yocto@5.10.21+gitAUTOINC+8c8f6a791b_8c516ced69-r0-3b09da53': 605,
 'linux-intel@5.10.1+gitAUTOINC+47c7a3148a_b8dd2066fa-r0-6f659545': 607,
 'weston-init@1.0-r0-4830f539': 608,
 'alsa-state@0.2.0-r5-ca13e115': 609,
 'ca-certificates@20210119-r0-b251b07d': 611,
 'psplash@0.1+gitAUTOINC+0a902f7cd8-r15-f3d6e7f9': 612,
 'bluez5@5.55-r0-72e49ea1': 613,
 'alsa-topology-conf@1.2.1-r0-48d39048': 712,
 'autoconf@2.69-r11-5623258b': 713,
 'automake@1.16.1-r0-d9fd36df': 714,
 'avahi@0.7-r0-b0325caa': 715,
 'babeltrace@1.5.8-r0-7c9e1501': 716,
 'babeltrace2@2.0.2-r0-ac7f94be': 717,
 'bash-completion@2.10-r0-b256c88f': 718,
 'bison@3.5.4-r0-65363863': 719,
 'blktrace@1.2.0+gitAUTOINC+cca113f2fe-r0-d9cb5343': 720,
 'bluez5@5.55-r0-bc964800': 721,
 'bzip2@1.0.8-r0-2f0de417': 722,
 'cairo@1.16.0-r0-5fa4392e': 723,
 'cantarell-fonts@0.0.25-r0-acd5ed2a': 724,
 'ccache@3.7.7-r0-78e9ed99': 725,
 'dbus@1.12.20-r0-48a836ce': 726,
 'diffstat@1.63-r0-9ffb0458': 727,
 'diffutils@3.7-r0-0c83a3d0': 728,
 'expat@2.2.9-r0-b264868b': 729,
 'findutils@4.7.0-r0-fbf6706a': 730,
 'flac@1.3.3-r0-de2ccc40': 731,
 'flex@2.6.4-r0-7756c0b7': 732,
 'gdbm@1.18.1-r0-b21bb0fc': 733,
 'gettext@0.20.1-r0-b4bdae56': 734,
 'gnome-desktop-testing@2018.1-r0-f627e9ae': 735,
 'gnu-efi@3.0.11-r0-0e90417b': 736,
 'gobject-introspection@1.62.0-r0-3671c6b6': 737,
 'grep@3.4-r0-a230b5f2': 738,
 'icu@66.1-r0-db2b6f0f': 739,
 'iptables@1.8.4-r0-9f6505a8': 740,
 'jansson@2.13.1-r0-c069be34': 741,
 'less@551-r0-8376bd3b': 742,
 'libcheck@0.14.0-r0-5153fb0b': 743,
 'libcroco@0.6.13-r0-da13c804': 744,
 'libgcrypt@1.8.5-r0-ea378016': 745,
 'libgudev@233-r0-d2f08613': 746,
 'libical@3.0.7-r0-08ad2de1': 747,
 'libice@1.0.10-r0-e3acc04a': 748,
 'libmpc@1.1.0-r0-4a81e037': 749,
 'libnewt@0.52.21-r0-044d7137': 750,
 'libogg@1.3.4-r0-eecc68e8': 751,
 'libpthread-stubs@0.4-r0-25f99154': 752,
 'libsamplerate0@0.1.9-r1-c275b692': 753,
 'libsm@1.2.3-r0-1b146605': 754,
 'libtool@2.4.6-r0-2598a745': 755,
 'libubootenv@0.3.1-r0-fe7146cc': 756,
 'liburcu@0.11.1-r0-d3748ea1': 757,
 'libvorbis@1.3.6-r0-0f723056': 758,
 'libxml2@2.9.10-r0-034e47bf': 759,
 'linux-stm32mp@5.10.10-r0-24a745d8': 760,
 'lttng-modules@2.11.6-r0-3ec642de': 761,
 'lz4@1.9.2-r0-92c5188f': 762,
 'm4@1.4.18-r0-bcff5266': 763,
 'mdadm@4.1-r0-2b19e0f6': 764,
 'mpfr@4.0.2-r0-28da39d3': 765,
 'openssl@1.1.1l-r0-9b547b47': 766,
 'patch@2.7.6-r0-920f37de': 767,
 'pkgconfig@0.29.2+gitAUTOINC+edf8e6f0ea-r0-6bd2b7e9': 768,
 'powertop@2.10-r0-7d83994a': 769,
 'python3-dbus@1.2.16-r0-6a4c7b8b': 770,
 'python3-pycairo@1.19.0-r0-5de2815a': 771,
 'quilt@0.66-r0-37a8b3a4': 772,
 'rauc@1.5.1-r0-293da210': 773,
 'sed@4.8-r0-9478ce3d': 774,
 'slang@2.3.2-r0-5592934c': 775,
 'socat@1.7.3.4-r0-8bd3a629': 776,
 'squashfs-tools@4.4-r0-b9829aa0': 777,
 'strace@5.5-r0-da431cf1': 778,
 'vala@0.46.6-r0-573c4ac0': 779,
 'wayland-protocols@1.20-r0-edc9f66f': 780,
 'xcb-proto@1.13-r0-3dfd5f25': 781,
 'xorgproto@2019.2-r0-b8a10c0e': 782,
 'xrandr@1.5.1-r0-37d3239f': 783,
 'xtrans@1.4.0-r0-8646bd64': 784,
 'zip@3.0-r2-e1addc30': 785}
```

```python
uploads2process["inbox-priority"]

{'base-files@3.0.14-r89-de7b0461': 634,
 'bind@9.11.32-r0-b855718c': 635,
 'busybox@1.31.1-r0-e95eb6a2': 636,
 'curl@7.69.1-r0-740d32f0': 637,
 'dnsmasq@2.81-r0-91fe6c10': 638,
 'gnupg@2.2.27-r0-8529d1eb': 640,
 'gnutls@3.6.14-r0-5dcb8af0': 641,
 'gcc@11.2.0-r0-cdb0ad07': 643,
 'libgcc@11.2.0-r0-cdb0ad07': 644,
 'libndp@1.7-r0-b91653fc': 645,
 'libsolv@0.7.10-r0-72c0e7cd': 646,
 'libx11@1.6.9-r0-177820de': 647,
 'linux-asos@5.10.61+gitAUTOINC+3b283fa8d4_452ea6a15e-r0-7d072497': 648,
 'linux-firmware@20210511-r0-02b99cf0': 649,
 'linux-intel@5.10.1+gitAUTOINC+47c7a3148a_b8dd2066fa-r0-356e247e': 650,
 'linux-raspberrypi@5.4.72+gitAUTOINC+5d52d9eea9_154de7bbd5-r0-f62adbb6': 651,
 'linux-seco@5.10.35+gitAUTOINC+5c9049b0d8-r0-886743a5': 652,
 'musl@1.2.2+gitAUTOINC+aad50fcd79-r0-13c36b71': 653,
 'networkmanager@1.22.10-r0-d47e6c86': 654,
 'nspr@4.25-r0-423ec5e8': 655,
 'nss@3.51.1-r0-59e9f324': 656,
 'ohos-googletest@0.0+gitAUTOINC+5433771038-r0-810cf672': 657,
 'optee-client@3.13.0.imx-r0-d564e8d0': 658,
 'optee-os@3.13.0.imx-r0-9e8d8102': 659,
 'python3@3.8.11-r0-a9a8f277': 660,
 'systemd@244.5-r0-e08425c0': 661,
 'tar@1.32-r0-05be69c9': 662,
 'util-linux@2.35.1-r0-a213bba5': 663,
 'weston@8.0.0-r0-0d4372b2': 664,
 'weston-init@1.0-r0-850d6130': 665,
 'wireless-regdb@2021.04.21-r0-555bd46d': 666,
 'wpa-supplicant@2.9-r0-5bc07e88': 667,
 'x-mounts@1.0-r0-d2143ddf': 668,
 'zephyr-philosophers@2.6.0+gitAUTOINC+837ab4a915_c3bd2094f9-r0-61e841e0': 669,
 'argp-standalone@1.3-r0-7feafb0f': 671,
 'binutils@2.37-r0-47d047a8': 672,
 'bsd-headers@1.0-r0-46c3d3fa': 673,
 'btrfs-tools@5.4.1-r0-59462737': 674,
 'curl@7.69.1-r0-e3503597': 675,
 'dbus-test@1.12.20-r0-8a11d160': 676,
 'dnsmasq@2.81-r0-b59711a7': 677,
 'encodings@1.0.5-r2.1-92dbbfcd': 678,
 'font-util@1.3.2-r0-73cfefc0': 679,
 'gdk-pixbuf@2.40.0-r0-2fc2fd75': 680,
 'gnu-config@20200117+gitAUTOINC+5256817ace-r0-0e6d0c21': 681,
 'go-runtime@1.14.15-r0-6b8c8c0e': 682,
 'libsndfile1@1.0.28-r0-f6314838': 683,
 'libssp-nonshared@1.0-r0-d8d416de': 684,
 'linux-libc-headers@5.4-r0-6b2b0e44': 685,
 'linux-oniro@5.10.61+gitAUTOINC+3b283fa8d4_452ea6a15e-r0-7d072497': 686,
 'linux-raspberrypi@5.10.63+gitAUTOINC+e0147386e9_4117cba235-r0-903a264e': 687,
 'lttng-tools@2.11.5-r0-0701005b': 688,
 'lttng-ust@2.11.2-r0-e1f1ba4c': 689,
 'make@4.3-r0-66565325': 690,
 'mkfontscale@1.2.1-r0-7e69fabb': 691,
 'musl-obstack@1.1-r0-e05f54b5': 692,
 'musl-utils@20170421-r0-b6886749': 693,
 'nettle@3.5.1-r0-f9384562': 694,
 'nss@3.51.1-r0-05dd441d': 695,
 'oniro-mounts@1.0-r0-d2143ddf': 696,
 'oniro-sysctl@1.0-r0-0939b30d': 697,
 'psplash@0.1+gitAUTOINC+0a902f7cd8-r15-5f3bde65': 698,
 'ptest-runner@2.4.0+gitAUTOINC+834670317b-r0-5c080deb': 699,
 'python3-pygobject@3.34.0-r0-a59c4834': 700,
 'sysota@git-r0-60f15869': 701,
 'systemd@244.5-r0-237e5339': 702,
 'tzdata@2021a-r0-c21ca93f': 703,
 'u-boot-seco-imx@2021.04-r0-ba00cae3': 704,
 'u-boot-stm32mp@2020.10.r1-r0-efee56e5': 705,
 'update-rc.d@0.8-r0-03c9539d': 706,
 'util-macros@1.19.2-r0-22e99a50': 707,
 'weston@8.0.0-r0-5311ce61': 708,
 'weston-init@1.0-r0-11bdcaca': 709,
 'xserver-xorg@1.20.8-r0-5283ef47': 710,
 'newlib@3.2.0-r0-488114c0': 711}
```


```python
upload_ids = str(tuple([ v for _, v in uploads2process["working/deb"].items() ]))

sql = f"""
-- 1 delete clearing decisions
DELETE FROM clearing_decision cd WHERE cd.clearing_decision_pk IN
(
   SELECT DISTINCT cde.clearing_decision_fk
   FROM clearing_decision_event cde
   WHERE cde.clearing_event_fk IN
   (
       SELECT DISTINCT ce.clearing_event_pk
       FROM clearing_event ce, uploadtree_a ut
       WHERE
           ut.uploadtree_pk = ce.uploadtree_fk
           AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
           AND ut.upload_fk IN {upload_ids}
   )
);

-- 2 delete entries in clearing decision event table

DELETE FROM clearing_decision_event cde
WHERE cde.clearing_event_fk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN {upload_ids}
);

-- 3 delete entries in clearing event table

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN {upload_ids}
);

-- 4 delete license file entries

DELETE FROM license_file lf1 WHERE lf1.fl_pk IN
(
 SELECT DISTINCT lf.fl_pk
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN {upload_ids}
   AND lf.agent_fk = a.agent_pk
   AND a.agent_name = 'reportImport'
);


-- 5 delete reportimport_ars records for upload

DELETE FROM reportimport_ars WHERE upload_fk IN {upload_ids};
"""

with open("deb_query.sql", "w") as f:
    f.write(sql)


```

query file: [deb_query.sql](data/deb_query.sql)

backup, as usual:

```bash
sudo service fossology stop
sudo service apache2 stop
sudo su - postgres
```

```sql
CREATE DATABASE fossologybaknew2 WITH TEMPLATE fossology OWNER fossy;
\q
```

```bash
exit
sudo service fossology start
sudo service apache2 start
```

**_20211102 Edit_: the sql query templates above have been modified after applying the queries here below; particularly, the condition `AND ce.removed = FALSE` has been removed from all queries because it prevents deleting all clearing events by reportImport, that can also *remove* licenses, not only add them

applying the query

```bash
sudo su - postgres
psql < deb_query.sql > deb_query.log
```

[deb_query1.sql](data/deb_query.sql)

[deb_query.log](data/deb_query.log)


check if it's necessary to recover something from the "old" pool in the old testing VM

```python
from glob import glob

for uploadname in uploads2process["working/deb"]:
    uploadname = uploadname.replace("@", "-")
    if not glob(f"/build/common/a4fpool/userland/*/*/{uploadname}.aliensrc"):
        print(f"{uploadname} is missing in pool")
```

output is:

```
wpa-supplicant-2.9-r0-6cf418a6 is missing in pool
bind-9.11.22-r0-0f274724 is missing in pool
librepo-1.11.2-r0-0470dea1 is missing in pool
dropbear-2019.78-r0-58f1e2b7 is missing in pool
```

looked in the current pool, only dropbear-2019.78-r0 is completely missing (so it needs to be fully imported), while the other packages are all present, even if with a different variant (so I'm importing only the missing `.aliensrc` file to make the upload command work.

```bash
alberto@ohos-build:/build/common/a4fpool$ tar -cvzf tmp_export.tgz userland/wpa-supplicant/2.9-r0/wpa-supplicant-2.9-r0-6cf418a6.aliensrc userland/bind/9.11.22-r0/bind-9.11.22-r0-0f274724.aliensrc userland/librepo/1.11.2-r0/librepo-1.11.2-r0-0470dea1.aliensrc userland/dropbear/2019.78-r0/*
userland/wpa-supplicant/2.9-r0/wpa-supplicant-2.9-r0-6cf418a6.aliensrc
userland/bind/9.11.22-r0/bind-9.11.22-r0-0f274724.aliensrc
userland/librepo/1.11.2-r0/librepo-1.11.2-r0-0470dea1.aliensrc
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.aliensrc
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json.bak
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.alienmatcher.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.deltacode.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.scancode.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.scancode.spdx
userland/dropbear/2019.78-r0/dropbear-2019.78.tar.bz2.alien.spdx
userland/dropbear/2019.78-r0/history/
userland/dropbear/2019.78-r0/history/20211013-151615_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210722-122206_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210722-122205_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210914-154930_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210914-154930_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210715-191409_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210902-111029_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210902-111029_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210713-080902_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210818-184742_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20211021-133701_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20211007-121006_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210716-124403_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210715-191410_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210716-124403_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210818-184743_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210705-205759_dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json
userland/dropbear/2019.78-r0/history/20210729-160442_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210729-160441_dropbear-2019.78-r0-58f1e2b7.final.spdx
```

then on the new machine

```bash
ubuntu@ip-172-31-19-84:/build/common/a4fpool$ tar -xvzf tmp_export.tgz
userland/wpa-supplicant/2.9-r0/wpa-supplicant-2.9-r0-6cf418a6.aliensrc
userland/bind/9.11.22-r0/bind-9.11.22-r0-0f274724.aliensrc
userland/librepo/1.11.2-r0/librepo-1.11.2-r0-0470dea1.aliensrc
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.aliensrc
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json.bak
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.alienmatcher.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.deltacode.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.scancode.json
userland/dropbear/2019.78-r0/dropbear-2019.78-r0.scancode.spdx
userland/dropbear/2019.78-r0/dropbear-2019.78.tar.bz2.alien.spdx
userland/dropbear/2019.78-r0/history/
userland/dropbear/2019.78-r0/history/20211013-151615_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210722-122206_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210722-122205_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210914-154930_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210914-154930_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210715-191409_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210902-111029_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210902-111029_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210713-080902_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210818-184742_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20211021-133701_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20211007-121006_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210716-124403_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210715-191410_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210716-124403_dropbear-2019.78-r0-58f1e2b7.final.spdx
userland/dropbear/2019.78-r0/history/20210818-184743_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210705-205759_dropbear-2019.78-r0-58f1e2b7.tinfoilhat.json
userland/dropbear/2019.78-r0/history/20210729-160442_dropbear-2019.78-r0-58f1e2b7.fossy.json
userland/dropbear/2019.78-r0/history/20210729-160441_dropbear-2019.78-r0-58f1e2b7.final.spdx
```

create an a4f session file with all the required packages

```bash
a4f session -c -s deb_reimport

...

21-10-26 19:21:24 DEBUG    aliens4friends.commons.session      | Session data written to 'sessions/deb_reimport.session.json'.
```


```python
import json
from aliens4friends.models.session import SessionModel, SessionPackageModel

with open("/build/common/a4fpool/sessions/deb_reimport.session.json") as f:
    j = json.load(f)

session = SessionModel.decode(j)

for uploadname in uploads2process["working/deb"]:
    parts = uploadname.split("@")
    name = parts[0]
    version = parts[1][:-9]
    variant = parts[1][-8:]
    session.package_list.append(SessionPackageModel(name, version, variant))

with open("/build/common/a4fpool/sessions/deb_reimport.session.json", "w") as f:
    f.write(session.to_json())
```

regenerate all `.debian.spdx` and `.alien.spdx` files in the pool (but first add missing snapmatch files)

(pool cache on)

```bash
a4f snapmatch -s deb_reimport
```

(pool cache off)

```bash
a4f spdxdebian -s deb_reimport
a4f spdxalien -s deb_reimport
```

(pool cache on)

```bash
a4f upload -s deb_reimport --folder 'working/deb'
```

Things did not go as expected for some uploads. Let's dig into it, by starting with package "bind".

This is an entry in `.debian.spdx`

```yml
# File

FileName: ./bin/pkcs11/pkcs11-destroy.c
SPDXID: SPDXRef-file-fe13fec279e0151a550e98d16c262958
FileChecksum: SHA1: 28fa5ace191c82093fae3c3ded1d6251b82b8659
LicenseConcluded: ISC AND BSD-2-Clause AND BSD-3-Clause AND BSD-4-Clause
LicenseInfoInFile: NOASSERTION
FileCopyrightText: NONE

```

and this is the corresponding entry in the alien `.scancode.spdx`

```yml
# File

FileName: ./bin/pkcs11/pkcs11-destroy.c
FileChecksum: SHA1: 9c2cb41b411390967dbeafea247a77a2320e02d7
LicenseConcluded: NOASSERTION
LicenseInfoInFile: BSD-2-Clause
LicenseInfoInFile: ISC
FileCopyrightText: <text>Copyright (c) 2009, 2015 Internet Systems Consortium, Inc.
Portions copyright (c) 2008 Nominet UK.
</text>
```

and this is the corresponding entry in '.alien.spdx`

```yml
# File

FileName: ./bin/pkcs11/pkcs11-destroy.c
SPDXID: SPDXRef-file-fe13fec279e0151a550e98d16c262958
FileChecksum: SHA1: 9c2cb41b411390967dbeafea247a77a2320e02d7
LicenseConcluded: NOASSERTION
LicenseInfoInFile: BSD-2-Clause
LicenseInfoInFile: ISC
FileCopyrightText: NONE
```

-----------------------------------------------------------------------
*Date: Nov 8, 2021*


The Audit Team asked to delete all scancode license findings (not debian decisions) from the inbox folder. Done with

```sql
DELETE FROM license_file lf1 WHERE lf1.fl_pk IN
(
 SELECT DISTINCT lf.fl_pk
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN (562, 563, 565, 566, 567, 568, 569, 570, 571, 572, 574, 575, 576, 577, 578, 579, 584, 585, 586, 587, 588, 589, 590, 593, 595, 596, 598, 599, 600, 601, 602, 603, 604, 605, 607, 608, 609, 611, 612, 613, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785)
   AND lf.agent_fk = a.agent_pk
   AND a.agent_name = 'reportImport'
);
```

for some reason clearing decisions by old imports were not deleted before importing the new decisions from debian filtered with the new logic.

Making a test on "bind" package in inbox:

```sql
DELETE FROM clearing_decision_event cde
WHERE cde.clearing_event_fk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk = 576
       AND ce.date_added < '2021-11-02 00:00:00.000000+00'
);

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk = 576
       AND ce.date_added < '2021-11-02 00:00:00.000000+00'
);
```

test gone OK, doing in on all other uploads in `inbox`:

```sql
DELETE FROM clearing_decision_event cde
WHERE cde.clearing_event_fk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN (562, 563, 565, 566, 567, 568, 569, 570, 571, 572, 574, 575, 576, 577, 578, 579, 584, 585, 586, 587, 588, 589, 590, 593, 595, 596, 598, 599, 600, 601, 602, 603, 604, 605, 607, 608, 609, 611, 612, 613, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785)
       AND ce.date_added < '2021-11-02 00:00:00.000000+00'
);

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN (562, 563, 565, 566, 567, 568, 569, 570, 571, 572, 574, 575, 576, 577, 578, 579, 584, 585, 586, 587, 588, 589, 590, 593, 595, 596, 598, 599, 600, 601, 602, 603, 604, 605, 607, 608, 609, 611, 612, 613, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785)


       AND ce.date_added < '2021-11-02 00:00:00.000000+00'
);
```

Deleting clearing events without deleting corresponding decisions may have the unwanted side effect of having a lot of "closed" files even without any actual decision event (so "falsely closed" files).

With this query, we delete all clearing decision records for files that do not have any clearing event associated with them any more, so everything is consistent again

```sql
DELETE FROM clearing_decision cd1 WHERE cd1.uploadtree_fk IN (

SELECT ut.uploadtree_pk FROM uploadtree_a ut
WHERE ut.upload_fk IN (562, 563, 565, 566, 567, 568, 569, 570, 571, 572, 574, 575, 576, 577, 578, 579, 584, 585, 586, 587, 588, 589, 590, 593, 595, 596, 598, 599, 600, 601, 602, 603, 604, 605, 607, 608, 609, 611, 612, 613, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768, 769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785)
AND NOT EXISTS (SELECT ce.clearing_event_pk FROM clearing_event ce WHERE ce.uploadtree_fk = ut.uploadtree_pk)
AND EXISTS (SELECT cd.clearing_decision_pk FROM clearing_decision cd WHERE cd.uploadtree_fk = ut.uploadtree_pk)

)
```

Sometimes reportImport does not create clearing events of type 4 (IMPORT) but of type 3 (AGENT) making it impossible to distinguish them based on type. So the only way is to checking datetime of reportImport job, and create a query like this:

```sql
DELETE FROM clearing_decision_event cde
WHERE cde.clearing_event_fk IN (

SELECT ce.clearing_event_pk FROM clearing_event ce, uploadtree_a ut
WHERE ut.uploadtree_pk = ce.uploadtree_fk
AND ut.upload_fk = 761
AND ce.date_added >= '2021-10-23 08:44'
AND ce.date_added <= '2021-10-23 08:50'
);

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN (

SELECT ce.clearing_event_pk FROM clearing_event ce, uploadtree_a ut
WHERE ut.uploadtree_pk = ce.uploadtree_fk
AND ut.upload_fk = 761
AND ce.date_added >= '2021-10-23 08:44'
AND ce.date_added <= '2021-10-23 08:50'
);

DELETE FROM clearing_decision cd1 WHERE cd1.uploadtree_fk IN (

SELECT ut.uploadtree_pk FROM uploadtree_a ut
WHERE ut.upload_fk = 761
AND NOT EXISTS (SELECT ce.clearing_event_pk FROM clearing_event ce WHERE ce.uploadtree_fk = ut.uploadtree_pk)
AND EXISTS (SELECT cd.clearing_decision_pk FROM clearing_decision cd WHERE cd.uploadtree_fk = ut.uploadtree_pk)

);
```


---------------------------------------------------------------------
*Date: Mar 4, 2022*

The Audit Team asked me to remove all scancode findings in OpenHarmony/v3.0-LTS folder.

Done with:

```sql
DELETE FROM license_file lf1 WHERE lf1.fl_pk IN
(
 SELECT DISTINCT lf.fl_pk
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN (878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896, 897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049, 1050, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079, 1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099, 1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129, 1130, 1131, 1132)
   AND lf.agent_fk = a.agent_pk
   AND a.agent_name = 'reportImport'
);
```

------------------------------------------------------------------------
*Date: May 11, 2022*

with the new Goofy uploads, the same problem popped up again, coming from old reportImport agent runs on Jasmine uploads, that may have some files in common with new Goofy uploads (to understand why this is happening, read #5 ).
We may have to delete all reportImport agent findings with timestamp dated 2020/2021, a query to select them may be as follows

```sql
SELECT DISTINCT lf.*
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk =295
   AND lf.agent_fk = a.agent_pk
   AND lf.rf_timestamp < '2022-01-01'
   AND a.agent_name = 'reportImport'
```

After creating an internal db backup:

```sql
DELETE FROM license_file lf1 WHERE lf1.fl_pk IN
(

SELECT DISTINCT lf.fl_pk
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN (2, 293, 294, 295, 297, 299, 300, 301, 304, 306, 308, 310, 311, 313, 315, 318, 319, 321, 322, 323, 324, 330, 331, 333, 334, 335, 339, 340, 341, 342, 343, 344, 347, 348, 349, 350, 354, 355, 356, 358, 361, 363, 364, 365, 370, 372, 375, 376, 377, 378, 379, 380, 383, 384, 385, 386, 387, 389, 390, 391, 394, 395, 397, 398, 400, 404, 405, 411, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 426, 427, 429, 430, 433, 434, 437, 438, 441, 442, 443, 444, 446, 447, 449, 451, 453, 454, 455, 456, 459, 461, 464, 466, 467, 468, 469, 470, 471, 473, 474, 478, 479, 480, 481, 484, 485, 486, 487, 493, 497, 499, 505, 508, 510, 512, 562, 565, 566, 567, 568, 569, 572, 577, 585, 587, 588, 593, 595, 596, 601, 602, 603, 609, 634, 641, 643, 644, 645, 647, 653, 655, 658, 659, 663, 667, 671, 672, 673, 674, 675, 678, 679, 680, 681, 683, 684, 685, 688, 689, 690, 691, 692, 693, 694, 696, 697, 699, 700, 702, 705, 706, 707, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 746, 747, 748, 749, 750, 751, 752, 753, 754, 756, 757, 758, 759, 760, 761, 762, 764, 765, 766, 767, 768, 769, 770, 771, 772, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 791, 793, 794, 796, 797, 798, 800, 801, 802, 803, 805, 806, 807, 808, 809, 810, 819, 820, 822, 823, 824, 860, 861, 862, 867, 868, 869, 870, 871, 872, 873, 874, 875)
   AND lf.agent_fk = a.agent_pk
   AND lf.rf_timestamp < '2022-01-01'
   AND a.agent_name = 'reportImport'
);
```
