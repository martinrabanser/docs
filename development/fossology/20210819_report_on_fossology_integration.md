---
title: Report on Fossology integration in the compliance toolchain
author: Alberto Pianon <pianon@array.eu>
date: "2021-08-19"
lastmod: "2021-08-19"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# 1. The Context

**Fossology** is at the **center of our IP compliance workflow**, since it is the place where all copyright and license metadata from automated scanners and the debian matching tool are **reviewed** and **validated** by the **audit team**.

We decided to add two external data sources to Fossology:

 - ScanCode (as an additional automated scanner, that adds some **substantial improvements** with respect Fossology's internal scanners, especially on **copyright notice detection**),
 - metadata from corresponding packages (if any) in the Debian distribution as a trusted source of truth (that allows us to **save a substantial amount of work by the audit team**, because we simply reuse the work made by Debian package maintainers).

We needed an interface to import such external tool data into Fossology; **such interface is *key* to our workflow**, for the reasons explained above.

The ready-to-use interface we decided to use since the very beginning is Fossology's `reportImport` agent, which takes SPDX format as input: we pack together results from Scancode (as `LicenseInfoInFile`[^licinfo]) and from the debian matcher (as `LicenseConcluded`[^licconcl]) into a single SPDX file and we upload it to Fossology.

  [^licinfo]: because they come from an *automated* tool (no human validation)
  [^licconcl]: because they come from *human* decisions by Debian package maintainers

The expected results were the following:

1. Scancode license findings are correctly imported into Fossology and are displayed in the Fossology UI as findings coming from the `reportImport` agent

![](img/scancode_finding.png)

2. Debian license findings are correctly imported as human/auditor decisions into Fossology, and are displayed in the Fossology UI as "Imported decisions" by `reportImport`.

![](img/imported_decision.png)

3. Scancode and Debian *copyright* findings are correctly imported into Fossology and are displayed in the Fossology UI as "user findings".

![](img/copyright.png)

The expected drawbacks of this approach were the following:

a) With Fossology's internal scanners you can highlight the exact lines of the source file where the scanner found the license statement (so it's easier for the auditor to validate the results) while the same is not possible with findings imported from Scancode because the import format (SPDX) cannot handle such data (i.e. license line numbers)

![](img/highlight.png)

b) Results (both from Scancode and from Debian) are all displayed as coming from the `reportImport` agent; but since Debian findings are marked as "Imported decisions", while ScanCode findings are *not* (as explained above), it should be still possible to distinguish them in Fossology UI, even if it's not straighforward.

# 2. The Problem Raised by the Audit Team (2021-08-17 meeting)

Unfortunately, in some months of practical use of the solution described above, our audit team found out some unexpected drawbacks, mainly due to `reportImport` agent bugs.

The main issue is that for some files `reportImport` randomly adds in Fossology a lot of strange license findings and decisions that are not present in the imported SPDX file, and are completely wrong or misleading.

![](img/main_bug.png)

Beacause of this bug, combined with drawbacks *a)* and *b)* above, it proves  hard to understand, on a case-by-case basis, if a wrong finding or a wrong decision comes from a false positive by ScanCode, by a mistake of the Debian package maintainer, or by the above-mentioned `reportImport` bug. This is causing a non-negligible loss of man hours.

Another drawback found by the audit team is that `reportImport` options are too limited, while it would be desirable to have more options and even to be able to set some rules in order to directly apply "Debian" decisions into Fossology only on certain conditions, and leave them as "to be discussed" if such conditions are not met, on a file-by-file basis.

![](img/reportImport.png)

Moreover, there is a further annoyance probably due to `reportImport`: when a Debian decision is imported, the automated scanner findings that do not match the decision are not displayed as removed from the "concluded license list" in the Fossology UI, even if the final SPDX BOM output by Fossology returns the correct result and displays only the Debian decision as `LicenseConcluded`: this inconsistency may easily confuse the auditor.

![](img/zconf_example.png)

```yaml
# File

FileName: ./zlib-1.2.11.tar.xz/zlib-1.2.11/zconf.h
SPDXID: SPDXRef-item1561159
FileChecksum: SHA1: 0ef05b0d12bf2cfbbf1aa84cff0e8dcf4fc5b731
LicenseConcluded: Zlib
LicenseInfoInFile: GPL-3.0-or-later
LicenseInfoInFile: LicenseRef-Zlib-possibility
LicenseInfoInFile: Zlib
FileCopyrightText: <text> Copyright (c) 1995-2016 Jean-loup Gailly, Mark Adler
Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler </text>
```

# 3. Steps Undertaken to Find a Solution (2021-08-18/19)

## 3.1. Fix reportImport?

The first natural option to explore was fixing `reportImport` bugs and adding some extra functionalities to it. However this does not seem a workable solution in the medium and long run for a number of reasons.

1. Based on git log, it appears that `reportImport` had been written in 2017 by a single developer (M. Huber) who is not actively contributing to Fossology any more, and the code has never been modified since then and it is not being maintained by anyone.

2. Fossology maintainer Gaurav Mishra confirmed us that `reportImport` is only rarely used and that there is no interest by the community in maintaining it, so if we are willing to fix and to enhance it, the burden would be in any case all upon us.

Given that `reportImport` still represents a compromise compared to our actual needs (see sec. 1), and was chosen at the very beginning only because it was the only ready-to-use interface available, instead of fixing and maintaining it with no community support it would make more sense to explore other options.

## 3.2. Use the new ScanCode agent.

There is an ongoing [Google Summer of Code Project sponsored by Siemens][project page], started in May 2021 and that is supposed to end by August 2021, aimed at fully integrating ScanCode into Fossology.

- An analysis of the code, of the available documentation and of the weekly progress reports available on the [project page],
- a live testing with real world data from our project,
- information collected from Fossology maintainer Gaurav Mishra (that told us it is very likely that this new agent will be officially released very soon and has attracted significant interest from the community)

all suggest that using this new agent to include Scancode data in our workflow could be the best option -- with an important *caveat* that will be explained later on.

  [project page]: https://fossology.github.io/gsoc/docs/2021/scancode/

By using the new agent there would be a substantial improvement on how Scancode data are imported and displayed on Fossology, with license text highlighting and the express indication that the finding comes from Scancode.

![](img/scancode_new.png)

However, in our early tests we found out a major issue, which luckily seems to be easily solvable -- but the **viability of this option depends on the actual solution to such issue**.

The following are the execution times of Scancode invoked via CLI (like we are currently doing in production) and via the new Fossology agent on package curl-7.69.1 (3155 files):

1. scancode CLI on the whole source dir with no multiprocessing: 18m:33s
2. scancode CLI on the whole source dir with 8 processes: 4m:20s
3. scancode run by Fossology agent (using 6 to 8 processes on average, based on `top` output, but scancode is invoked once for each file): 36m:36s !!!

Unfortunately, no. 3 would not be a workable solution in production for us. The scan of the first pre-release of our project with Scancode (~230 packages, ~1M files) took ~2 days even if we were using 16 cores with 32GB of RAM... using the Fossology agent it would have taken ~2 weeks!

The problem is that the new Fossology agent integrates Scancode by calling it file by file, in a way that is consistent with Fossology plugin architecture, but that is disastrous as to execution time, because Scancode has a long startup time (not only it loads many modules, but it has to load the whole license db in memory every time) and it is mainly intended to be run on a whole source directory and not on a single file.

Luckily, a similar problem (and a possible solution) had been raised in the Scancode github repo some time ago (https://github.com/nexB/scancode-toolkit/issues/1400#issuecomment-468609294) and a [POC solution](https://github.com/nexB/scancode-toolkit/commit/8afa686fb71b9540029234e5a40c0572c4457c28) has already been included in the [master branch](https://github.com/nexB/scancode-toolkit/blob/develop/etc/scripts/scanserv.README) even if it hasn't been further developed.

Basically, with a simple python script it could be possible to run a persistent scancode process that can accept multiple calls by a local client, which could be invoked by the Fossology agent on a file by file basis, with no startup overhead on each call (because everything would be loaded only once, when the persistent process is started).

In this way the current architecture and logic of the agent (that is very neat and clean, and consistent with Fossology's architecture) may remain unchanged (i.e. file-by-file processing), and development time and costs to get a workable solution would be hopefully low.

We already discussed this issue with Fossology maintainer Gaurav Mishra, we received a positive feedback, we have been invited to raise the issue in the current Pull Request ([that we promptly raised](https://github.com/fossology/fossology/pull/2074#issuecomment-902057799)),  and we are confident that the problem will be solved soon -- but again, **on this depends the viability of this solution, so we should actively collaborate** with Fossology's Team, if required.

## 3.3. Write a custom Fossology agent for our Debian Matcher

By using the new ScanCode/Fossology agent we would solve only one half of our probem: we still need a way to reliably import Debian findings into Fossology.

Given that there might be some community interest about it (especially from those using Yocto), it could make sense to write a custom Fossology agent to import Debian data or even a full wrapper for our Debian matcher tool, that could be fully integrated (in a way similar to Scancode) in Fossology.

Fossology documentation on agent/plugin development is outdated and incomplete (see https://github.com/fossology/fossology/wiki#general) and full information is available only at detail level in code comments (collected and made available [here](https://fossology.github.io/d9/d0d/libraries.html) for convenience); unfortunately, there is no general quickstart or how-to guide. However, there are some [agent examples to begin with](https://fossology.github.io/dd/d9c/exampleagents.html).

In theory, agents could be written in any language, but in practice fossology libraries are available only in C, C++ and PHP; if one is willing to use another language, they should access fossology file repository by invoking some dedicated CLI commands, and write their own library to access fossology db -- at their own risk.

So the actual options seem to be:

1. writing a full agent in C/C++, starting from available examples
2. writing a full agent in PHP, starting from simpler existing agents as examples
3. writing a wrapper agent (in C/C++ or PHP) for our debian matcher which should be invoked as an external CLI tool (we could use the new Scancode agent as a good example to start with)
4. write an importer agent (in C/C++ or PHP) that simply imports debian matcher results (with conditions and rules that can be set by users).

Options #1 and #2 are obviously unworkable. Option #3 may prove hard (also because of the debian snapshot timeout problem, that we still have to solve), but could attract more interest by the community. Option #4 seems the easiest one to develop, and would not even require substantial changes to our current workflow, even if it will likely remain an "internal" tool for our workflow, and will hardly attract any interest from the community.
