
-- 1 delete clearing decisions
DELETE FROM clearing_decision cd WHERE cd.clearing_decision_pk IN
(
   SELECT DISTINCT cde.clearing_decision_fk 
   FROM clearing_decision_event cde 
   WHERE cde.clearing_event_fk IN
   (
       SELECT DISTINCT ce.clearing_event_pk
       FROM clearing_event ce, uploadtree_a ut
       WHERE 
           ut.uploadtree_pk = ce.uploadtree_fk
           AND ce.removed = FALSE
           AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
           AND ut.upload_fk IN (292, 296, 301, 309, 310, 311, 314, 315, 320, 326, 327, 329, 330, 334, 339, 340, 341, 343, 344, 350, 352, 353, 358, 364, 371, 378, 380, 383, 384, 389, 393, 394, 397, 401, 470)
   )
);

-- 2 delete entries in clearing decision event table

DELETE FROM clearing_decision_event cde 
WHERE cde.clearing_event_fk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE 
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.removed = FALSE
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN (292, 296, 301, 309, 310, 311, 314, 315, 320, 326, 327, 329, 330, 334, 339, 340, 341, 343, 344, 350, 352, 353, 358, 364, 371, 378, 380, 383, 384, 389, 393, 394, 397, 401, 470)
);

-- 3 delete entries in clearing event table

DELETE FROM clearing_event ce1
WHERE ce1.clearing_event_pk IN
(
   SELECT DISTINCT ce.clearing_event_pk
   FROM clearing_event ce, uploadtree_a ut
   WHERE 
       ut.uploadtree_pk = ce.uploadtree_fk
       AND ce.removed = FALSE
       AND ce.type_fk = 4 -- ClearingDecisionTypes::IMPORT
       AND ut.upload_fk IN (292, 296, 301, 309, 310, 311, 314, 315, 320, 326, 327, 329, 330, 334, 339, 340, 341, 343, 344, 350, 352, 353, 358, 364, 371, 378, 380, 383, 384, 389, 393, 394, 397, 401, 470)
);

-- 4 delete license file entries

DELETE FROM license_file lf1 WHERE lf1.fl_pk IN 
(
 SELECT DISTINCT lf.fl_pk 
 FROM license_file lf, uploadtree_a ut, agent as a
 WHERE 
   lf.pfile_fk = ut.pfile_fk
   AND ut.upload_fk IN (292, 296, 301, 309, 310, 311, 314, 315, 320, 326, 327, 329, 330, 334, 339, 340, 341, 343, 344, 350, 352, 353, 358, 364, 371, 378, 380, 383, 384, 389, 393, 394, 397, 401, 470)
   AND lf.agent_fk = a.agent_pk
   AND a.agent_name = 'reportImport'
);


-- 5 delete reportimport_ars records for upload

DELETE FROM reportimport_ars WHERE upload_fk IN (292, 296, 301, 309, 310, 311, 314, 315, 320, 326, 327, 329, 330, 334, 339, 340, 341, 343, 344, 350, 352, 353, 358, 364, 371, 378, 380, 383, 384, 389, 393, 394, 397, 401, 470);
