---
title: How to setup a development playground for Oniro IP Compliance Toolchain
author: Alberto Pianon <pianon@array.eu>
date: 2022-08-17
lastmod: 2022-08-17
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# Context

The peculiarity of the Oniro IP Compliance Toolchain is that we need to build and scan a whole complex build matrix every time, as better explained [here](https://git.ostc-eu.org/oss-compliance/docs/-/blob/main/audit_workflow/what_Oniro_is_all_about.md#22-automating-ip-scanning-of-a-complex-build-matrix-a-dedicated-toolchain), [here](https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat#tinfoilhat) and, more in detail, [here](https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat/-/blob/master/docs/recipe_variants.md).

So to develop any new tool or functionality to add to the toolchain, developers need a playground with "real" data where to experiment and test their solutions.

# Requirements and installation

To this purpose, we suggest to set up a dedicated machine with at least 16 cores, 32GB RAM, 1TB ext4 volume mounted on /build, and Ubuntu 20.04.4, through the attached [script](./setup_dev_playground.sh), based on the official [Oniro CI docker image] dockerfile (please note that no user should be created before running the script: you should use a clean Ubuntu 20.04 base server installation).

The reason to use Ubuntu 20.04 is that such distro is the recommended distro to build Oniro, and it is used in the official [Oniro CI docker image]. Since all CI docker images used by IP Compliance pipelines are built on top of such image (because such pipelines have to build Oniro as first necessary step), and since any new tool or functionality is supposed to be installed/added to Compliance CI docker images when ready for production, it makes sense to test any new tool or functionality on the same environment.

[Oniro CI docker image]: https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/blob/kirkstone/.oniro-ci/containers/bitbake-builder/Dockerfile

You should copy the script to your local machine, make it executable, and run it. You do not need docker; any reference to the official oniro builder dockerfile in the script's comments is just to acknowledge that the installation steps performed by the script are taken from there.

# How to play

If the installation script successfully completed, you should have a working build environment for Oniro with 

- `builder` (uid 1000) as the user to be "used" for all tests and operations
- all needed scripts (`oniro-workspace.sh`, `yoctobuild.sh`, `tinfoilhat`, `aliensrc_creator`)
- the following directory structure:
    - `/build/oniro`: where builds will actually happen, and relevant `.env` and `.log` files will be stored
    - `/build/common/downloads` and `/build/common/sstate-cache`: common download and build cache to be shared across builds (it dramatically reduces build time after the first time)
    - `/build/tmp`: temporary directory to be used by the above scripts

## Only once (just the first time)

Save common environment variables in a `.env` file in `builder` home dir:

```bash
su - builder
cd ~

echo "
CI_PROJECT_NAME=oniro
CI_PROJECT_URL=https://gitlab.eclipse.org/eclipse/oniro-core/oniro
PROJECT=oniro
BUILD_DIR=/build
TMPDIR=/build/tmp
MANIFEST_URL=https://gitlab.eclipse.org/eclipse/oniro-core/oniro
MANIFEST_NAME=default.xml
MANIFEST_BRANCH=kirkstone
BUILD_TAG=linux
TEMPLATECONF=../oniro/flavours/linux
OE_INIT_BUILD_ENV=./oe-core/oe-init-build-env
" > ~/.env 
```

## Every time

```bash
su - builder
cd ~

export $(cat .env | xargs)

oniro-workspace.sh .env
```

> **NOTE**: if you want to work again with an already created workspace, skip the previous part and `cd` to the directory where the sources you want to work with have been cloned.

Then `cd` to the directory where the latest available sources have been cloned (see the last line of the previous command's output, you should get something like:
`CI: successfully cloned sources at /build/oniro/kirkstone-v2.0.0-alpha2-170-ge06072d`) and and set some specific environment variables:

```bash
cd /build/oniro/kirkstone-v2.0.0-<...>/

export $(cat .env | xargs)
# .env file locally created by the previous command
export BB_LOCAL_CONF='
DL_DIR = "/build/common/downloads"
SSTATE_DIR = "/build/common/sstate-cache"
BB_GENERATE_MIRROR_TARBALLS = "1"
'
export MACHINE=qemux86-64
export RECIPES="oniro-image-base oniro-image-extra"
# you can choose whatever target machine and image recipes here,
# look at https://git.ostc-eu.org/oss-compliance/pipelines/-/blob/master/oniro.yml for available options
```

then 

```
yoctobuild.sh
```

or, if your machine is a remote one, you have better do:

```
nohup yoctobuild.sh &

tail -f nohup.out
```

since the process may take many hours to complete.

After completion, you should find built binary images in `/build/oniro/kirkstone-<...>/build-linux-<machine>/tmp/deploy/images/<machine>`.

Happy hacking!
