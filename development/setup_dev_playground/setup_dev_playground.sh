#!/bin/bash

# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2022 Alberto Pianon <pianon@array.eu>
# SPDX-FileCopyrightText: 2021 Huawei Inc.

set -e

cp /etc/os-release .
chmod +x os-release
. "./os-release"
rm os-release
if [[ "$NAME $VERSION" != "Ubuntu 20.04.4 LTS (Focal Fossa)" ]]; then
  echo "This script must be run only in Ubuntu 20.04.4"
  exit 1
fi

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ -n "$(cat /etc/passwd | grep 1000:1000)" ]; then
	echo "User with uid 1000 has already been created on this system!"
	echo "Please use this script on a clean Ubuntu 20.04 installation"
	exit 1
fi

wget -P /etc/apt/sources.list.d/ \
 https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/raw/kirkstone/.oniro-ci/containers/bitbake-builder/ppa/zyga-ubuntu-oh-tools-focal.list
wget -P /etc/apt/trusted.gpg.d/ \
 https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/raw/kirkstone/.oniro-ci/containers/bitbake-builder/ppa/zyga-ubuntu-oh-tools.gpg

apt-get update \
 && apt-get install -y \
	bash git-repo git-lfs apt-utils build-essential chrpath cpio diffstat \
	gawk git sudo wget language-pack-en-base time locales python-is-python3 \
	python3-distutils python3-pip libssl-dev iproute2 iputils-ping curl jq \
	lz4 zstd iptables file \
 && apt-get install -y 'ca-certificates=20211016~20.04.1'

pip3 install anybadge

# Let's just have /bin/sh as bash
echo "dash dash/sh boolean false" | debconf-set-selections \
 && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

# runqemu command needs sudo permission to create tun device for the
# communication between host and vm for gcc testing.
useradd --create-home --uid 1000 --shell /usr/bin/bash builder \
 && usermod -aG sudo builder\
 && echo "builder ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

wget -P /home/builder \
  https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/raw/kirkstone/.oniro-ci/containers/bitbake-builder/.gitconfig

chown builder:builder /home/builder/.gitconfig

wget -P /usr/local/bin \
  https://git.ostc-eu.org/oss-compliance/pipelines/-/raw/master/scripts/oniro-workspace.sh \
	https://git.ostc-eu.org/oss-compliance/pipelines/-/raw/master/scripts/yoctobuild.sh

wget -O /usr/local/bin/tinfoilhat \
  https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat/-/raw/master/tinfoilhat.py

wget -O /usr/local/bin/aliensrc_creator \
  https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat/-/raw/master/aliensrc_creator.py

chmod +x /usr/local/bin/*

mkdir -p /build/oniro /build/common/downloads /build/common/sstate-cache /build/tmp

chown -R builder:builder /build/oniro /build/common /build/tmp

