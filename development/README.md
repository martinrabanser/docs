---
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Documentation about Oniro Compliance Toolchain development

- [Getting started](../deployment/getting_started.md)
- [Set up a development playground](./setup_dev_playground)
- Detail documentation may be found in each project's README:
	- [TinfoilHat](https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat)
	- [Aliens4Friends](https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends)
	- [Dashboard](https://github.com/noi-techpark/solda-aliens4friends-dashboard)
	- [CI Pipelines](https://git.ostc-eu.org/oss-compliance/pipelines)
- [BANG integration](./bang)
- [Fossology integration](./fossology)
- [SW360 integration](./sw360)
