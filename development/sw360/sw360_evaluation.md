---
title: Evaluation of SW360 for possible integration in the Oniro Compliance Toolchain
author: Alberto Pianon <pianon@array.eu>
draft: true
date: "2022-03-30"
lastmod: "2022-03-30"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---


# Why SW360

SW360 (an EF project) is an application to manage compliance-related information about third party software components used in multiple projects in complex organizations

https://www.eclipse.org/sw360/

https://github.com/eclipse/sw360

Even if our use case is different (a single - but complex - reference project whose compliance information need to be shared downstream - in "read-only mode" - with device makers) we should evaluate it as an OSS component inventory tool and possibly as a database backend for Aliens4Friend (sw360 uses both couchdb and postgresql, for different purposes, apparently).

In any case, a possible development may be a **data exporter from Aliens4Friends Db to sw360**, that may be used by device makers (and by us, to simulate the creation of ReadmeOSS files through sw360 with data provided by Aliens4Friends Db)

# Installation of a test instance of sw360

The recommended way of installing a test instance is via vagrant, but it may not work on VMs, so we fall back to docker installation (also available).

- Hetzner VM CPX51 (16 VCPU, 32GB RAM, 160GB storage)
- Ubuntu 20.04
- Docker version 20.10.8, build 3967b7d
- Docker Compose version v2.3.3

Tested git version sw360-15.0.0-M1-52-g67ed5bf2

Followed instructions found in `README_DOCKER.md` (with some adjustments and some hints from sw360 slack channel guys)

```bash
./docker-build.sh
docker-compose up -d
docker exec -it sw360-postgresdb-1 createdb -U liferay -W fossology # pwd: liferay
docker volume create fossy_srv
docker run \
    --network sw360net \
    -p 8081:80 \
    --name fossology \
    -e FOSSOLOGY_DB_HOST=postgresdb \
    -e FOSSOLOGY_DB_USER=liferay \
    -e FOSSOLOGY_DB_PASSWORD=liferay \
    -v fossy_srv:/srv \
    -d fossology/fossology
```

sw360 is reachable at http://localhost:8080 (the first time it takes a lot of time to open), with user `setup@sw360.org` and password `sw360fossy`

fossology is reachable at http://localhost:8081, user and password: `fossy`

## Deployment

Some operations need to be manually carried out in liferay webUI in order to apply the correct settings and actually install sw360 frontend:

- https://github.com/eclipse/sw360/wiki/Deploy-Liferay7.3#user-and-login-settings-in-liferay
- https://github.com/eclipse/sw360/wiki/Deploy-Liferay7.3#setup-sw360-for-liferay-import-lar-files (pay particular attention to the written instructions, the screenshots only partially show what needs to be selected)

## Importing User accounts for testing.

The setup account does not belong to a group. Thus, not all view are functional because they require a group membership to work correctly.

Instead of manually creating a set of users, sw360 comes with a dummy user file to be imported for testing purposes: https://github.com/eclipse/sw360/wiki/Deploy-Liferay7.3#import-user-accounts-for-testing

## Connect Fossology

https://github.com/eclipse/sw360/blob/master/README_DOCKER.md#configure-fossology

in case of use of `ufw`, we should use the internal ip address of the fossology docker container:

```shell
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' fossology
```

assuming that the output is 172.19.0.5, the url to be used to connect to fossology should be:

http://172.19.0.5:80/repo/api/v1/


# Deploying sw360 in a more production-like environment

server available from the Internet, ufw + SSL under Apache2 proxy

(in this case no dummy user list has been used)

## UFW and docker

There is a know issue of Docker bypassing UFW and exposing internal ports to the Internet: https://github.com/chaifeng/ufw-docker

In this specific case, the deployment through `docker-compose` would expose couchdb and postgresql ports, as well as http connections to sw360 and fossology at ports 8080 and 8081.

I adopted the solution described [here](https://github.com/chaifeng/ufw-docker#solving-ufw-and-docker-issues) adapting it to the specific network configuration of my Hetzner VM; so I appended to `/etc/ufw/after.rules`:

```bash
# BEGIN UFW AND DOCKER
*filter
:ufw-user-forward - [0:0]
:ufw-docker-logging-deny - [0:0]
:DOCKER-USER - [0:0]
-A DOCKER-USER -j ufw-user-forward

-A DOCKER-USER -j RETURN -s 188.34.203.211/32
-A DOCKER-USER -j RETURN -s 172.17.0.0/12
-A DOCKER-USER -j RETURN -s 172.20.0.0/12


-A DOCKER-USER -p udp -m udp --sport 53 --dport 1024:65535 -j RETURN

-A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 188.34.203.211/32
-A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 172.17.0.0/12
-A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 172.20.0.0/12
-A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 188.34.203.211/32
-A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 172.17.0.0/12
-A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 172.20.0.0/12

-A DOCKER-USER -j RETURN

-A ufw-docker-logging-deny -m limit --limit 3/min --limit-burst 10 -j LOG --log-prefix "[UFW DOCKER BLOCK] "
-A ufw-docker-logging-deny -j DROP

COMMIT
# END UFW AND DOCKER
```

then I restarted and configured UFW:

```bash
systemctl restart ufw
ufw allow ssh
ufw allow http
ufw allow https
ufw enable
```

(ports 80 and 443 are enabled here for future use with apache2 proxy)

## Apache2 Proxy

```bash
apt-get install apache2
a2enmod ssl
a2enmod headers
a2enmod rewrite
a2enmod proxy_http
a2ensite default-ssl
service apache2 restart
apt install certbot python3-certbot-apache
certbot --apache
nano /etc/apache2/sites-available/default-ssl.conf
```

added at the beginning:

```
<VirtualHost *:80>
    ServerName sw360.pianon.eu
    Redirect / https://sw360.pianon.eu
</VirtualHost>

<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
                ServerName sw360.pianon.eu
[...]
```

and at the end:

```
[...]
         # Fossology
         ProxyPass /repo http://127.0.0.1:8081/repo
         ProxyPassReverse /repo http://127.0.0.1:8081/repo

         # sw360
         ProxyPass / http://127.0.0.1:8080/
         ProxyPassReverse / http://127.0.0.1:8080/

         # needed to make REST API work
         # see https://github.com/eclipse/sw360/wiki/Dev-REST-API#known-problems
         RequestHeader set X-Forwarded-Proto: "https"

	</VirtualHost>
</IfModule>
```



then

```bash
a2dissite 000-default.conf
service apache2 restart
```
