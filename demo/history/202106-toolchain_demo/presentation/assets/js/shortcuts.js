module.exports = (markdown, options) => {
  return new Promise((resolve, reject) => {
    return resolve(
      markdown
        .split('\n')
        .map((line, index) => {
          return line.replace(/<!--\ ?bkg\ ?(.*)\.(jpg|png|svg|webp)\ ?-->/g, '<!-- .slide:  data-background-image="local/img/$1.$2" -->')
          .replace(/^\+(.*\.)(jpg|png|svg|webp)/g, '<!-- .slide:  data-background-image="local/img/$1$2" -->')
          .replace(/<!--\ ?frag(\ ?.*?)\ ?index ([0-9])\ ?-->/g, '&shy;<!-- .element: class="fragment$1" data-fragment-index="$2"-->')
          .replace(/<!--\ ?frag(\ ?.*?)-->/g, '&shy;<!-- .element: class="fragment$1" -->')
          .replace(/<!--\ ?center\ ?-->/g, '&shy;<!-- .element: class="center" -->')
          .replace(/<!--\ ?center-img\ ?-->/g, '&shy;<!-- .element: class="center-img" -->')
          .replace(/<!--\ ?left-img\ ?-->/g, '&shy;<!-- .element: class="left-img" -->')
          .replace(/<!--\ ?right-img\ ?-->/g, '&shy;<!-- .element: class="right-img" -->')
          .replace(/<!--\ ?center-img-large\ ?-->/g, '&shy;<!-- .element: class="center-img-large" -->')
          // attempt to make a column
          // FIXME: delete eventually
          //.replace(/<!--\ ?right-column\ ?-->/g, '&shy;<div class="right">')
          //.replace(/<!--\ ?left-column\ ?-->/g, '&shy;<div class="left">')
          // FIXME: until here
          // FIXME: delete right and left classes in source CSS, confusing
          // same using flex:
          .replace(/^<!--\ ?columns\ ?-->/g, '<div class="container">')
          .replace(/^<!--\ ?column\ ?-->/g, '<div class="col">')
          .replace(/^<!--\ ?right-column\ ?-->/g, '<div class="col">')
          .replace(/^<!--\ ?left-column\ ?-->/g, '<div class="col">')
          .replace(/^<!--\ ?end\ ?-->/g, '</div>')
          // .replace(/<!--\ ?id (.*?)\ ?-->/g, '<!-- .slide: id="$1" -->')
          .replace(/<!--\ ?id (.*)\ ?-->/g, '<!-- .slide: id="$1" -->')
          .replace(/@fa\[(.*)\ index ([0-9])\ ?\]/g, '<i class="fa fa-$1 fragment" data-fragment-index="$2"></i>')
          .replace(/@far\[(.*)\ index ([0-9])\ ?\]/g, '<i class="far fa-$1 fragment" data-fragment-index="$2"></i>')
          .replace(/@fa\[(.*?)\]/g, '<i class="fa fa-$1"></i>')
          .replace(/@far\[(.*?)\]/g, '<i class="far fa-$1"></i>');
        })
        .join('\n')
    );
  });
}
