# How to deploy a demo environment for Aliens4Friends

## Hardware Requirements

Many tools of the Aliens4Friends toolchain are very resource intensive, so it is bitbake; thus, a machine with at least 16GB of RAM and 8 cores is required, otherwise some steps of the workflow (especially ScanCode and Fossology agents) may take forever. If you want some more speed, go for 32GB of RAM and 16 cores.

The disk size depends on the size of the sources and of the build dirs you need to scan. For a large project like AllScenariOS, if you want to scan all available builds 1TB disk is recommended.

## Operating System.

Ubuntu 20.04 LTS is recommended (the demo environment may be deployed also in other Linux distros but some installation steps may need some changes).

## Docker

Since we will deploy Fossology in a docker container, you need docker installed. Choose the most appropriate installation method depending on your infrastructure.

## Bitbake and build infrastructure

Installation of bitbake/yocto and of the build system dependencies is not covered here. Please follow the [official guide](https://docs.oniroproject.org/en/latest/).

## Bitbake builds

You need one or more bitbake builds to scan to use Aliens4Friends.

You can build sources following the official guide, with an important exception: **do not use source mirrors nor SSTATE mirrors**, because otherwise the toolchain could not find sources and required build metadata.

As a reference, you can use the script [build_all.sh](./build_all.sh) (which assumes that you built bitbake sources within `/build/ohos`).

## Installation of required tools

Aliens4Friends requires various tools. For installation and execution
instruction follow the guidelines inside the main repository's
[Aliens4Friends/README.md](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends/-/blob/master/README.md).

