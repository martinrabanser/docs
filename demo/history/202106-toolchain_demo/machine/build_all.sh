#!/bin/bash

cd /build/ohos

repo init -u https://git.ostc-eu.org/OSTC/OHOS/manifest.git -b refs/tags/v0.1.0
repo sync


FLAVOUR=linux
MACHINES=(qemux86-64 qemux86 qemuarm qemuarm64 seco-intel-b68 stm32mp1-av96 seco-imx8mm-c61)
IMAGES=(openharmony-image-base openharmony-image-base-tests openharmony-image-extra openharmony-image-extra-tests)

for m in ${MACHINES[@]}; do
  for image in ${IMAGES[@]}; do
    TEMPLATECONF=../sources/meta-ohos/flavours/$FLAVOUR . ./sources/poky/oe-init-build-env build-$FLAVOUR-$m
    sed -i.bak -E "s/#MACHINE \?= \"$m\"/MACHINE \?= \"$m\"/" conf/local.conf
    case $m in
      seco-intel-b68)
        echo "CONFIG_SERIAL_OF_PLATFORM= \"y\"" >> conf/local.conf
        ;;
      seco-imx8mm-c61)
        echo "ACCEPT_FSL_EULA = \"1\"" >> conf/local.conf
        ;;
    esac
    # do not use cache
    sed -i \
       -e 's/^INHERIT += "own-mirrors"/#INHERIT \+= "own-mirrors"/' \
       -e 's/^SOURCE_MIRROR_URL/#SOURCE_MIRROR_URL/' \
       -e 's/^SSTATE_MIRRORS/#SSTATE_MIRRORS/' \
       conf/local.conf
    bitbake $image
    cd ..
  done
done

FLAVOUR=zephyr
MACHINES=(qemu-x86 qemu-cortex-m3 96b-nitrogen 96b-avenger96)
image=zephyr-philosophers

for m in ${MACHINES[@]}; do
  TEMPLATECONF=../sources/meta-ohos/flavours/$FLAVOUR . ./sources/poky/oe-init-build-env build-$FLAVOUR-$m
  sed -i.bak -E "s/#MACHINE \?= \"$m\"/MACHINE \?= \"$m\"/" conf/local.conf
    # do not use cache
  sed -i \
     -e 's/^INHERIT += "own-mirrors"/#INHERIT \+= "own-mirrors"/' \
     -e 's/^SOURCE_MIRROR_URL/#SOURCE_MIRROR_URL/' \
     -e 's/^SSTATE_MIRRORS/#SSTATE_MIRRORS/' \
     conf/local.conf
  bitbake $image
  cd ..
done

TEMPLATECONF=../sources/meta-ohos/flavours/freertos . ./sources/poky/oe-init-build-env build-freertos-qemuarmv5
# do not use cache
  sed -i \
     -e 's/^INHERIT += "own-mirrors"/#INHERIT \+= "own-mirrors"/' \
     -e 's/^SOURCE_MIRROR_URL/#SOURCE_MIRROR_URL/' \
     -e 's/^SSTATE_MIRRORS/#SSTATE_MIRRORS/' \
     conf/local.conf
bitbake freertos-demo
cd ..
