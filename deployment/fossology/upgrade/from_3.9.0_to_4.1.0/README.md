---
title: "Upgrade from Fossology 3.9.0 to 4.1.0: activity log and report"
author: Alberto Pianon <pianon@array.eu>
date: "2022-05-02"
lastmod: "2022-07-09"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# Upgrade from Fossology 3.9.0 to 4.1.0: activity log and report

- [x] 1. create a full backup of the current instance (only files and database)
- [x] 2. check all the customizations/fixes added to the current instance (also manually added ones): have they been implemented in the latest release? (check also for db tables changes - eg copyright?)
- [x] 3. check for an upgrade path
- [x] 4. install docker on the fossology machine
- [x] 5. install and test v.4.0.0 vanilla
- [x] 6. install and test v.4.1.0-rc1 + scancode with docker... doesn't work :(
- [x] 7. install and test v.4.1.0-rc1 + scancode from sources... works :)
- [x] 8. create a clone of the existing Fossology production machine in Array infrastructure (as a fallback in case something goes wrong)
- [x] 9. check for api calls to be updated in aliens4friends code
- [x] 10. create an internal copy of fossology production database
- [x] 11. try to upgrade production machine to v.4.1.0-rc1
  - [x] 11.1 change db encoding from SQL_ASCII to UTF8 using the slower method
  - [x] 11.2 follow installation/upgrade process described in the official wiki
- [x] 12. verify and check if everything works correctly
  - [x] 12.1 check if all previous uploads are visible
  - [x] 12.2 upload of zlib package and run all agents
  - [x] 12.3 generate spdx-tv
  - [x] 12.4 reportImport
  - [x] 12.5 reuse agent
- [x] 13. Check if system configuration tweaks are still applied
- [x] 14. Upgrade fossology-python package in aliens4friends (include it in docker image) and update and test api calls in a4f code
  - [x] 14.1 add (fix missing support for bblayer info)
  - [x] 14.2 match + filter (debian matching < 80 )
  - [x] 14.3 snapmatch (fix missing timeout handling)
  - [x] 14.4 sdpxdebian
  - [x] 14.5 spdxalien (add option to avoid import of scancode findings altogether)
    - [x] 14.5.1 check `.spdx.alien` files created with the new optionm
  - [x] 14.6 fossy
- [x] 15. implement missing customizations
  - [x] 15.1 job status API needs to be (re)fixed

--------------------------------------------------------------------------------

*Date: May 2, 2022*

# 1. create a full backup of the current instance (only files and database)

## 1.1. establish connection with compliance gitlab runner (where to store Fossology backup)

```shell
root@ecs-gitlab-runner-compliance:~# adduser fossy
```

```shell
root@fossology-priv:~# cat /home/fossy/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOCKW5HSzYX/8TzCZFPiGHMPzY3IxScxQLxsjQgdD6KiMGdVPuHES0c7UdUoys/XQvNZ7Z6oeyeAhNmsViM2fPfodBfId40uAUZ5QQLuak1qbEY11k4HfTCmtigWVGRTeNwWJrtaUFS/1PVeIp2hOOrFJmEeELT3c7kPxG3IGO22jEDM9apuGZUPJvGRyZ1ilHf2N5A1o2a0JOUTo0AcKn9OofYWdXmW18wY5tC5NjJ7kh8aB++1iWh89oolJqtx5KZvimEAb49XfwSc6N4gREPLs7iZy2qbv2AH7nPN+h03pb7r4pAbfrvok+6EL6rLWjvinxTacDCqSKCmno/w/v fossy@fossology-priv
```

```shell
fossy@ecs-gitlab-runner-compliance:~$ echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOCKW5HSzYX/8TzCZFPiGHMPzY3IxScxQLxsjQgdD6KiMGdVPuHES0c7UdUoys/XQvNZ7Z6oeyeAhNmsViM2fPfodBfId40uAUZ5QQLuak1qbEY11k4HfTCmtigWVGRTeNwWJrtaUFS/1PVeIp2hOOrFJmEeELT3c7kPxG3IGO22jEDM9apuGZUPJvGRyZ1ilHf2N5A1o2a0JOUTo0AcKn9OofYWdXmW18wY5tC5NjJ7kh8aB++1iWh89oolJqtx5KZvimEAb49XfwSc6N4gREPLs7iZy2qbv2AH7nPN+h03pb7r4pAbfrvok+6EL6rLWjvinxTacDCqSKCmno/w/v fossy@fossology-priv" \
  >> .ssh/authorized_keys
```

```shell
root@fossology-priv:~# echo "192.168.XXX.XXX   ecs-gitlab-runner-compliance" >> /etc/hosts
root@fossology-priv:~# ssh -i /home/fossy/.ssh/id_rsa fossy@ecs-gitlab-runner-compliance # test ssh connection and accept server key
fossy@ecs-gitlab-runner-compliance:~$ exit
```

## 1.2 Create backup dir

```shell
root@ecs-gitlab-runner-compliance:~# mkdir -p /build/backup/fossy
root@ecs-gitlab-runner-compliance:~# chown fossy:fossy /build/backup/fossy/
```

## 1.3 Modify Fossology backup script

In `fossology-priv` machine, modified `/fossology/utils/backup/fo-backup` env vars as follows:

```bash
# user variables
BACKUPDIR=/build/backup/fossy              # Directory on the backup server to store backup data
KEY=/home/fossy/.ssh/id_rsa                # ssh private key about backup user 'fossy'
SOURCEDIR=/srv/fossology/                  # Source directory to backup
PGBACKUPDIR=/srv/fossology/db              # Directory to store database backup data
BACKUPUSER=fossy@ecs-gitlab-runner-compliance        # Backup user and hostname/ip
EXCLUDES=(./backup_include_a ./backup_include_b)     # Exclude files to exclude or include backup directory and files
```

and patched line 161 by adding the missing `-` after `su`

```bash
   su - postgres -c 'pg_dumpall' | gzip > $PGBACKUPDIR/fo_dbbackup.gz
```

## 1.4 Run backup

```shell
root@fossology-priv:~/backup# nohup ./fo-backup -v &
```

# 2. check all the customizations/fixes added to the current instance

## 2.1 Code changes

see [install_fossology_on_debian_10.sh](../../installation/install_fossology_on_debian_10.sh)

| our manual change to 3.9.0 | implemented in 4.0.0 |
| ------ | ------ |
| PATCHING EASYRDF TO IMPORT BIG SPDX FILES (bugfix backport from v1.1.1 to v.0.9.0) | yes, easyrdf upgraded to 1.1.1 |
| PATCHING REST API to correctly report job status (see [issue](https://github.com/fossology/fossology/issues/1800#issuecomment-71291978)) | **no, PR is still [open](https://github.com/fossology/fossology/pull/1955)** |
| Backporting fix to Reuser agent from Fossology 3.10 | yes |


## 2.2 DB structure changes

None

## 2.3 system configuration

| config | applicable also to 4.0.0 | already in docker image? |
| ------ | ------ | ------ |
| [Adjusting the Kernel](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#adjusting-the-kernel) | yes | n.a. |
| [Tweaking postgresql](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#preparing-postgresql) | yes | no |
| [PHP configuration](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#configuring-php) (see [here](../../installation/install_fossology_on_debian_10.sh) line 155) | yes | no **(our values are higher)** |
| mail configuration (see [here](../../installation/install_fossology_on_debian_10.sh) line 167) | yes | no |


# 3. Check for an upgrade path

It seems that the only relevant change in db is the adding of the copyright_event table in 3.10.0

https://github.com/fossology/fossology/releases

https://github.com/fossology/fossology/tree/4.0.0/install/db

So migration from 3.9.0 to 4.0.0 should work without intermediate steps

# 4. install docker on fossology machine

```shell
apt-get update
apt-get install     ca-certificates     curl     gnupg     lsb-release
 curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
docker run hello-world
```

# 5. Install and test Fossology 4.0.0 Vanilla

```shell
a2enmod proxy
a2enmod proxy_http

cd /etc/apache2/sites-available
cp default-ssl.conf default-ssl-8443.conf
nano default-ssl-8443.conf
```

at the beginning, modified:

```
<IfModule mod_ssl.c>
        <VirtualHost _default_:8443>
```

at the end:

```
        SSLProxyEngine On
        ProxyRequests On
        ProxyPreserveHost On

        ProxyPass /repo http://127.0.0.1:8080/repo
        ProxyPassReverse /repo http://127.0.0.1:8080/repo

        </VirtualHost>
</IfModule>
```

then

```shell
nano /etc/apache2/ports.conf
```

```
<IfModule ssl_module>
        Listen 443
        Listen 8443
</IfModule>
```

```shell
docker run -d -p 8080:80 fossology/fossology:4.0.0
service apache2 restart
```

test instance is now accessible at https://fossology-priv:8443

## 5.1 Checking available agents...

![](img/screenshot01.png)

**Anomalies detected**

- all agents' version/revision is unknown...
- scancode agent is missing

# 6. install and test v.4.1.0-rc1 + scancode

after removing the previous container:

```
docker run -d -p 8080:80 --name fossy_scancode fossology/fossology:4.1.0-rc1
```

![](img/screenshot02.png)

still agents' revision is unknown, but scancode agent is present. Scancode command isn't, though:

![](img/screenshot03.png)

trying to install it withing docker container, as a test:

```shell
docker exec -it fossy_scancode bash
```

```shell
apt-get update && apt-get install -y python3-dev libbz2-1.0 xz-utils zlib1g libxml2-dev libxslt1-dev libpopt0
su - fossy
wget https://github.com/nexB/scancode-toolkit/releases/download/v30.1.0/scancode-toolkit-30.1.0_py37-linux.tar.xz
tar xJf scancode-toolkit-30.1.0_py37-linux.tar.xz
cd scancode-toolkit-30.1.0/
./scancode --help
exit
ln -s /home/fossy/scancode-toolkit-30.1.0/scancode /usr/local/bin/scancode
exit
```

```shell
docker restart fossy_scancode
```

however, in this way scancode agents hangs forever, until scheduler is restarted (and in the latter case it fails with no clear error)

![](img/screenshot04.png)

--------------------------------------------------------------------------------

*Date: May 3, 2022*

this trick finally works:

```bash
docker run -d -p 8080:80 --name fossy_scancode fossology/fossology:4.1.0-rc1
docker exec -it fossy_scancode bash
```

```bash
apt-get update && apt-get install -y python3-dev libbz2-1.0 xz-utils zlib1g libxml2-dev libxslt1-dev libpopt0
python3 -m pip install --upgrade pip setuptools wheel
python3 -m pip install scancode-toolkit

# workaround for fossology setting HOME=/root when invoking scancode
mv /usr/local/bin/scancode /usr/local/bin/scancode-cli
echo '#!/bin/bash
export HOME=/home/fossy
scancode-cli $@
' > /usr/local/bin/scancode
chmod +x /usr/local/bin/scancode

# workaround to get rid of (a lot of) annoying warnings in scancode logs
echo "import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
$(cat /usr/local/lib/python3.7/dist-packages/scancode/cli.py)
" > /usr/local/lib/python3.7/dist-packages/scancode/cli.py
```

however, the "unknown" revision of fossology agents may be a serious issue for data cleanness

![](img/screenshot05.png)

Let's try installation from sources, then

Installing fossology from souces in Debian 10, following the official wiki instructions, scancode works out of the box and the "unknown" revision problem goes away (tried it on a temporary machine in my infrastructure)

![](img/screenshot06.png)

# 9. Upgrade fossology-python package in aliens4friends (include it in docker image) and update api calls in the code

# 9.1 List of api calls in the code

| method or object | changed in latest version |
| ------ | ------ |
| `fossology.list_jobs(upload=upload, page_size=2000)` | yes, but it returns a tuple (list of jobs, number of pages); added all_pages: get all jobs |
|  `Job.queueDate`, `Job.status` |  apparently the same   |
| `fossology.rootFolder` | apparently the same |
| `fossology.create_folder(parent_obj, name)` | apparently the same |
| `fossology.list_uploads()` | added a lot of options, probably the one to use is all_pages=True, it returns a tuple (list of uploads, number of pages) |
| `Upload` | same, just added filesize and filesha1 |
| `fossology.detail_folder(upload.folderid)` (returns folder obj) | same |
| `Folder` | same |
| `fossology.schedule_jobs(folder, upload, specs, wait=True)` | same (can scancode be scheduled here?) |
| `fossology.upload_summary(upload)` | same, added group (optional) |
| `Summary` |  same |
| `fossology.session.get` (used to get licenses and raw json summary, not using python wrapper method) | same, but json output schema slightly changes (licenses has `findings/scanner` and `findings/conclusion` instead of `agentFindings` and `conclusions`)  |
| `fossology.api` | same |
| `fossology.generate_report(upload=upload,report_format=ReportFormat.SPDX2TV)` (returns rep_id) | same, added group (optional) |
| `fossology.download_report(rep_id)` | same, added group (optional) |

### 9.1.1 In particular: json schema differences for upload licenses and summary

| api call  | 3.9.0 | 4.1.0-rc1 |
| ------- | ------ | ------ |
| summary | ![](img/screenshot07.png) | ![](img/screenshot08.png) |
| licenses | ![](img/screenshot09.png) | ![](img/screenshot10.png) |


## 9.2 Used WebUI Functionalities not exposed in API

| functionality | changes in 4.1.0 | added to API in 4.1.0 |
| ------ | ------ | ------ |
| reportImport | apparently none | no |
| rename upload | apparently none | no |

--------------------------------------------------------------------------------

*Date: May 4, 2022*

## 10. create an internal copy of fossology production database

as root:

```bash
service fossology stop
service apache2 stop
su - postgres -c psql
```

```sql
CREATE DATABASE fossologybak20220504 WITH TEMPLATE fossology OWNER fossy;
\q
```

### 11.1 change db encoding from SQL_ASCII to UTF8

this is the issue: https://github.com/fossology/fossology/wiki/Migration-to-UTF-8-DB

```bash
cd /fossology
git checkout master
git pull
git checkout 4.1.0-rc1
make -C src/copyright/agent fo_unicode_clean
su - postgres
pg_dump fossology > 20220504_fossology.sql
/fossology/src/copyright/agent/fo_unicode_clean -i 20220504_fossology.sql -o 20220504_fossology_encoded.sql
dropdb fossology
createdb -O "fossy" fossology
psql -d fossology --file=20220504_fossology_encoded.sql
exit
```

success, apparently:

![](img/screenshot11.png)

## 11.2 follow installation/upgrade process described in the official wiki

```shell
cd /fossology
utils/fo-cleanold
utils/fo-installdeps

# workaround to get rid of (a lot of) annoying warnings in scancode logs
echo "import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
$(cat /usr/local/lib/python3.7/dist-packages/scancode/cli.py)
" > /usr/local/lib/python3.7/dist-packages/scancode/cli.py

make
make install
/usr/local/lib/fossology/fo-postinstall
systemctl daemon-reload
service fossology restart
```

output of `fo-postinstall`:

```
*** Running postinstall for everything ***
*** Running postinstall for common actions***
*** Creating user and group ***
NOTE: group 'fossy' already exists, good.
NOTE: user 'fossy' already exists, good.
*** Making sure needed dirs exist with right ownership/permissions ***
*** clearing file cache ***
NOTE: Repository already exists at /srv/fossology/repository
NOTE: Running the PostgreSQL vacuum and analyze command can result in a large database performance improvement.
      We suggest that you either configure postgres to run its autovacuum and autoanalyze daemons, or maintagent -D in a cron job, or run Admin > Maintenance on a regular basis.
      Admin > Dashboard will show you the last time vacuum and analyze have been run.
*** Running postinstall for web-only actions***
*** Setting up the FOSSology database ***
NOTE: fossology database already exists, not creating
*** Checking for plpgsql support ***
NOTE: plpgsql already exists in fossology database, good
*** Checking for 'uuid-ossp' support ***
NOTE: 'uuid-ossp' doesn't exist, adding
CREATE EXTENSION
*** update the database and license_ref table ***
Old release was 3.0.1
  Applying database functions
DB schema has been updated for fossology.
Database schema update completed successfully.
Update reference licenses
Migrate: Add and setup column=is_enabled to table=copyright_decision
Migrate: Add and setup column=is_enabled to table=ecc_decision
*** Instance UUID ***INSTANCE UUID Empty - creating...
INSTANCE UUID: e3744f6c-13f7-4494-ba5f-433bdc291654
*** Inserting 98566 records from copyright to copyright_event table ***
Inserted 0 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 0 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 43606 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 82686 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98531 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98533 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98535 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98537 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98539 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98541 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98543 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98545 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98547 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98549 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98551 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98553 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98555 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98557 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98559 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98561 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98563 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98565 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98567 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
*** Table author already migrated to author_event table ***
*** Table ecc already migrated to ecc_event table ***
*** Table keyword already migrated to keyword_event table ***

install python dependencies
*** Installing Debian runtime python dependencies ***
Reading package lists... Done
Building dependency tree       
Reading state information... Done
python3-pip is already the newest version (18.1-5).
python3 is already the newest version (3.7.3-1).
The following packages were automatically installed and are no longer required:
  linux-image-4.19.0-12-amd64 linux-image-4.19.0-13-amd64
Use 'apt autoremove' to remove them.
0 upgraded, 0 newly installed, 0 to remove and 53 not upgraded.
Database connectivity is good.
*** Setting up scheduler ***
*** Setting up the web interface ***
NOTE: Adding user www-data to group fossy
Fossology already enabled for apache2
[ ok ] Reloading apache2 configuration (via systemctl): apache2.service.
FOSSology postinstall complete, but sure to complete the remaining
  steps in the INSTALL instructions.
```

--------------------------------------------------------------------------------

*Date: May 5, 2022*

# 12. verify and check if everything works correctly

## 12.1 upload of zlib package and run all agents

Tested with zlib 1.2.11, it seems that ojo agent failed to compile correctly

![](img/screenshot12.png)

![](img/screenshot13.png)

tried:

```bash
cd /fossology
make -C src/ojo clean
make -C src/ojo
make -C src/ojo install
```

works!

![](img/screenshot14.png)

## 12.2 generate spdx-tv

apparently works

![](img/screenshot15.png)

## 12.3 reportImport

trying to import the spdx.rdf report just generated, it apparently works:

![](img/screenshot16.png)

![](img/screenshot17.png)

## 12.3 reuse agent

reuse agent apparently worked, but after that no upload is visible any more in browse mode, even if all uploads are still there in the database and if I try to schedule a new reuse job, I can see all of them in dropdown menu

![](img/screenshot18.png)

![](img/screenshot19.png)

![](img/screenshot20.png)

it seems a permission problem:

![](img/screenshot21.png)

Going back to point 11, and trying to rebuild everything from scratch (deleting the existing fossology source folder first, which may contain old build artifacts)

```bash
service fossology stop
service apache2 stop
cd /
rm -Rf fossology/
git clone https://github.com/fossology/fossology.git
cd fossology
git checkout 4.1.0-rc1
utils/fo-cleanold
utils/fo-installdeps -y

su - postgres
dropdb fossology
createdb -O "fossy" fossology
psql -d fossology --file=20220504_fossology_encoded.sql
exit

make
make install
/usr/local/lib/fossology/fo-postinstall
systemctl daemon-reload
service fossology restart
service apache2 start
```

output of fo-postinstall:

```
*** Running postinstall for everything ***
*** Running postinstall for common actions***
*** Creating user and group ***
NOTE: group 'fossy' already exists, good.
NOTE: user 'fossy' already exists, good.
*** Making sure needed dirs exist with right ownership/permissions ***
*** clearing file cache ***
NOTE: Repository already exists at /srv/fossology/repository
NOTE: Running the PostgreSQL vacuum and analyze command can result in a large database performance improvement.
      We suggest that you either configure postgres to run its autovacuum and autoanalyze daemons, or maintagent -D in a cron job, or run Admin > Maintenance on a regular basis.
      Admin > Dashboard will show you the last time vacuum and analyze have been run.
*** Running postinstall for web-only actions***
*** Setting up the FOSSology database ***
NOTE: fossology database already exists, not creating
*** Checking for plpgsql support ***
NOTE: plpgsql already exists in fossology database, good
*** Checking for 'uuid-ossp' support ***
NOTE: 'uuid-ossp' doesn't exist, adding
CREATE EXTENSION
*** update the database and license_ref table ***
Old release was 3.0.1
  Applying database functions
DB schema has been updated for fossology.
Database schema update completed successfully.
Update reference licenses
Migrate: Add and setup column=is_enabled to table=copyright_decision
Migrate: Add and setup column=is_enabled to table=ecc_decision
*** Instance UUID ***INSTANCE UUID Empty - creating...
INSTANCE UUID: 730677c2-2454-4b36-a828-388b0c3f72be
*** Inserting 98566 records from copyright to copyright_event table ***
Inserted 0 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 0 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 43606 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 82686 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98531 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98533 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98535 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98537 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98539 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98541 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98543 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98545 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98547 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98549 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98551 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98553 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98555 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98557 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98559 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98561 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98563 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98565 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
Inserted 98567 / 98566 rows to copyright_event table in 00 minutes and 00 seconds.
*** Table author already migrated to author_event table ***
*** Table ecc already migrated to ecc_event table ***
*** Table keyword already migrated to keyword_event table ***

install python dependencies
*** Installing Debian runtime python dependencies ***
Reading package lists... Done
Building dependency tree       
Reading state information... Done
python3-pip is already the newest version (18.1-5).
python3 is already the newest version (3.7.3-1).
The following packages were automatically installed and are no longer required:
  linux-image-4.19.0-12-amd64 linux-image-4.19.0-13-amd64
Use 'apt autoremove' to remove them.
0 upgraded, 0 newly installed, 0 to remove and 53 not upgraded.
Database connectivity is good.
*** Setting up scheduler ***
*** Setting up the web interface ***
NOTE: Adding user www-data to group fossy
Fossology already enabled for apache2
FOSSology postinstall complete, but sure to complete the remaining
  steps in the INSTALL instructions.
```

The problem with upload permissions is still there, so likely I didn't notice it the previous time...

Trying to upload a new package, and see which permission code it gets in `perm_upload` table

uploaded zlib to test folder, I can see it and it has id 1134

![](img/screenshot22.png)

However, it does not seem to have any different permission from the other ones:

![](img/screenshot23.png)

likely it's a problem with db conversion from SQL_ASCII to UTF8 **or** a problem with db migration, and it doesn't involve perm_upload table.

I took the query used [here](https://github.com/fossology/fossology/blob/a3c93461b11aefc5a6b5d0ceb4a4a34ed8621cee/src/lib/php/common-folders.php#L392) to get all uploads from a certain folder, regardless of permissions:

```sql
SELECT upload_pk, upload_desc, upload_ts, upload_filename
	FROM foldercontents,upload
  INNER JOIN uploadtree ON upload_fk = upload_pk AND upload.pfile_fk = uploadtree.pfile_fk AND parent IS NULL AND lft IS NOT NULL
	WHERE foldercontents.parent_fk = '59'
	AND foldercontents.foldercontents_mode = 2
	AND foldercontents.child_id = upload.upload_pk
	ORDER BY upload_filename,upload_pk;
```

('59' is 'Jasmine' folder id)

In db backup `fossologybak20220504` (SQL_ASCII) the query returns the correct results, while in new (re-encoded) `fossology` db it returns nothing

going back (again!)

```bash
service fossology stop
service apache2 stop

su - postgres
dropdb fossology
createdb -O "fossy" fossology
psql -d fossology --file=20220504_fossology_encoded.sql
psql
```

```
\c fossology
SELECT upload_pk, upload_desc, upload_ts, upload_filename
	FROM foldercontents,upload
  INNER JOIN uploadtree ON upload_fk = upload_pk AND upload.pfile_fk = uploadtree.pfile_fk AND parent IS NULL AND lft IS NOT NULL
	WHERE foldercontents.parent_fk = '59'
	AND foldercontents.foldercontents_mode = 2
	AND foldercontents.child_id = upload.upload_pk
	ORDER BY upload_filename,upload_pk;
```

result is:

```
 upload_pk | upload_desc | upload_ts | upload_filename
-----------+-------------+-----------+-----------------
(0 rows)
```

while with

```
\c fossologybak20220504
```

the same query returns 288 rows.

So it's a db re-encoding problem

going back (another time...) and performing re-encoding using an alternative (slower) method documented [here](https://github.com/fossology/fossology/wiki/Migration-to-UTF-8-DB#fixing-existing-db-slow)

```bash
service fossology stop
service apache2 stop

su - postgres
dropdb fossology
psql
```

restoring the old (SQL_ASCII-(un)encoded) database:

```
CREATE DATABASE fossology WITH TEMPLATE fossologybak20220504 OWNER fossy;
\q
```

```
exit
cd /fossology
/usr/local/lib/fossology/fo-postinstall --force-encode
```

# 12. verify and check if everything works correctly (again)

## 12.1 check if all previous uploads are visible (again)

yes!

![](img/screenshot24.png)


## 12.2 upload of zlib package and run all agents (again)

success!

![](img/screenshot25.png)

## 12.3 generate spdx-tv (again)

works!

![](img/screenshot26.png)

## 12.4 reportImport (again)

ok

![](img/screenshot27.png)

![](img/screenshot28.png)

## 12.5 reuse agent (again)

works as expected, all files are cleared

![](img/screenshot29.png)

![](img/screenshot30.png)

# 13. Check if system configuration tweaks are still applied

| config | still applied? | action taken |
| ------ | ------ | ------ |
| [Adjusting the Kernel](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#adjusting-the-kernel) | yes | none |
| [Tweaking postgresql](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#preparing-postgresql) | yes | none |
| [PHP configuration](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#configuring-php) (see [here](../../installation/install_fossology_on_debian_10.sh) line 155) | yes | none |
| [mail configuration](see [here](../../installation/install_fossology_on_debian_10.sh) line 167) | yes | none |

# 14. Upgrade fossology-python package in aliens4friends (include it in docker image) and update and test api calls in a4f code

made changes to aliens4friends/commons/fossywrapper.py, testing on gitlab runner machine with the latest builds available:

```shell
a4f add -s kirkstone-v1.0.0-rc-340-gc066689 /build/oniro/kirkstone-v1.0.0-rc-340-gc066689/tinfoilhat/* /build/oniro/kirkstone-v1.0.0-rc-340-gc066689/aliensrc/*
a4f match -s kirkstone-v1.0.0-rc-340-gc066689
a4f session --filter score-gt=80 -s kirkstone-v1.0.0-rc-340-gc066689
a4f snapmatch -s kirkstone-v1.0.0-rc-340-gc066689
a4f scan -s kirkstone-v1.0.0-rc-340-gc066689
a4f spdxdebian -s kirkstone-v1.0.0-rc-340-gc066689
a4f spdxalien -s kirkstone-v1.0.0-rc-340-gc066689 --skip-scancode-licenses
cd /a4fpool/userland
for f in $(find . -name *.alien.spdx -newermt 2022-05-02); do less $f; done
```

--------------------------------------------------------------------------------

*Date: May 26, 2022*

# (Follow-Up)

after about 3 weeks of tests, it seems that everything work as expected, apart from one issue that involves e2fsprogs and bzip2 packages, where the ununpack job seems to take forever and make the aliens4friend pipeline hang. This will be investigated in a separate issue and then reported back here

--------------------------------------------------------------------------------

*Date: June 24, 2022*

The problem is that both source packages, for different reasons, have internal images/archives, used only for testing, which contain thousands and thousands of files; since Fossology expands any possible image/archive found in uploaded sources, those packages end up in having an incredibly huge amount of files, which render Fossology unusable on such uploads.

I implemented special workarounds for both packages above:

https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/-/merge_requests/63

https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/-/commit/c2688dcec6fa1114f6fffa4e80226bd9d27d4f5b

--------------------------------------------------------------------------------

*Date: Jul 9, 2022*

# 15. implement missing customizations

# 15.1 job status API needs to be (re)fixed

(see first table [here above](#note_65731))

as root:

```bash
cd /usr/local/share/fossology/www/ui/api/Controllers/
cp JobController.php JobController.php.bak
patch -p1 << EOT
--- a/JobController.php
+++ b/JobController.php
@@ -228,24 +228,25 @@
     \$status = "";
     \$jobqueue = [];

+    \$sql = "SELECT jq_pk from jobqueue WHERE jq_job_fk = \$1;";
+    \$statement = __METHOD__ . ".getJqpk";
+    \$rows = \$this->dbHelper->getDbManager()->getRows(\$sql, [\$job->getId()],
+      \$statement);
     /* Check if the job has no upload like Maintenance job */
     if (empty(\$job->getUploadId())) {
-      \$sql = "SELECT jq_pk, jq_end_bits from jobqueue WHERE jq_job_fk = \$1;";
-      \$statement = __METHOD__ . ".getJqpk";
-      \$rows = \$this->dbHelper->getDbManager()->getRows(\$sql, [\$job->getId()],
-        \$statement);
       if (count(\$rows) > 0) {
-        \$jobqueue[\$rows[0]['jq_pk']] = \$rows[0]['jq_end_bits'];
-      }
-    } else {
-      \$jobqueue = \$jobDao->getAllJobStatus(\$job->getUploadId(),
-        \$job->getUserId(), \$job->getGroupId());
+        \$jobqueue[] = \$rows[0]['jq_pk'];
+      }
+    } else {
+      foreach(\$rows as \$row) {
+        \$jobqueue[] = \$row['jq_pk'];
+      }
     }

     \$job->setEta(\$this->getUploadEtaInSeconds(\$job->getId(),
       \$job->getUploadId()));

-    \$job->setStatus(\$this->getJobStatus(array_keys(\$jobqueue)));
+    \$job->setStatus(\$this->getJobStatus(\$jobqueue));
   }

   /**
EOT
```

It worked!
