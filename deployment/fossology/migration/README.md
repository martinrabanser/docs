---
title: Fossology Migration
author: Alberto Pianon <pianon@array.eu>
date: "2022-07-01"
lastmod: "2022-07-01"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# Fossology migration

<!-- TOC -->

- [1. Overview and migration strategy](#1-overview-and-migration-strategy)
	- [1.1. General information about FOSSology](#11-general-information-about-fossology)
	- [1.2. Features](#12-features)
  - [1.3. Architecture](#13-architecture)
  - [1.4. Migration strategy](#14-migration-strategy)
- [2. Fossology deployment and testing (v4.1.0)](#2-fossology-deployment-and-testing-v410)
	- [2.1. Installation](#21-installation)
		- [2.1.1. Prerequisites](#211-prerequisites)
		- [2.1.2. Quick installation script](#212-quick-installation-script)
	- [2.2. Testing](#22-testing)
	- [2.3. Testing VPN access by real users](#23-testing-vpn-access-by-real-users)
- [3. Backup and restore (data migration)](#3-backup-and-restore-data-migration)
	- [3.1. First "dirty" backup of the old Fossology instance](#31-first-dirty-backup-of-the-old-fossology-instance)
	- [3.2. First "dirty" restore on the "new" Fossology server.](#32-first-dirty-restore-on-the-new-fossology-server)
	- [3.3. "Migration Day": clean backup and restore](#33-migration-day-clean-backup-and-restore)

<!-- /TOC -->

## 1. Overview and migration strategy

### 1.1. General information about FOSSology

[FOSSology](https://www.fossology.org/) is an open source license compliance software system and toolkit, initially developed by HP as an internal project and then released as FLOSS (the first published version dates back to 2007). It is currently maintained by a wide and active community, and the [development](https://github.com/fossology/fossology) is mainly backed by Siemens and other companies using it.

While a lot of [documentation](https://github.com/fossology/fossology/wiki) is available (even if not always complete or up-to-date), given the complexity of the project it is worth it to give a general overview of Fossology's features, design and deployment, putting together pieces of information that are scattered all over the official documentation, and based also on our usage experience over the years.

### 1.2. Features

Fossology incorporates a number of **automated license and copyright scanners** (that extract copyright and license information **from source code files**), but its key feature is its **WebUI**, through which a Compliance Audit Team can collaboratively work to **review and validate the automated scanner results** (that usually have a lot of false positives and false negatives) and generate SPDX SBOM, audit reports, and Readme_OSS files for each source component. The review can be carried out both manually (file by file) and by using bulk text recognition rules set by the auditor/user.

![](img/clean-zlib.png)

Last but not least, past audit work on previous versions of the same source components may be automatically reused on newer versions, speeding up review time.

![](img/reuser.png)

Under the hood, Fossology has a scheduler that is used both to launch scanner jobs on uploaded source components, and to run bulk recognition jobs created by auditors/users.

![](img/jobs.png)

For integration with other applications, Fossology has a [Rest API](https://www.fossology.org/get-started/basic-rest-api-calls/
) through which many operations can be automated to ease the work of the Audit Team (i.e. source components upload, launching of automated scanners, report generation, and so on).

Through Rest API, Fossology may be also used just as a license scanner scheduler and SPDX SBOM generator, without any human interaction, but it is not the way it designed to be used, because all the (fundamental) human review would be missing, and generated SBOM data would be generally inaccurate and full of incorrect information.

### 1.3. Architecture

**Fossology WebUI and scheduler** are both written in **PHP**, and use a **postgres db** to store data.

Uploaded **source archives are stored** (unpacked) in the **filesystem**, typically under `/srv/fossology`, with a hashed directory structure (like in `1c/15/0c/1c150cd355f79e5c5b73cf8e90915e741b1fb671`; actual file names are stored in the database).

Both the database and the file storage are expected to grow significantly with a regular usage (by and large, tens of GB for the database, and hundreds of GB for the storage).

**Fossology WebUI runs on Apache**, while **the scheduler runs in a separate service** (named "**fossology**"). The WebUI runs independently from the scheduler, but when the scheduler service is stopped or dead, no job can be scheduled and only manual operations can be performed.

**Code scanners** ("agents", in Fossology's parlance) are **independent executables** and may be virtually written in any language; however, a layer written in PHP or in C/C++ is needed to interact with the database and the scheduler making use of [Fossology libraries](https://github.com/fossology/fossology/tree/master/src/lib).

So there are some **"native" agents**, directly written in PHP or C/C++; and some **"ported" agents**, where an independent license scanner (eg. Scancode) is integrated/invoked via an additional layer written in PHP or C/C++.

### 1.4. Migration strategy

A "live" Fossology migration to another server might be **challenging**, especially if the other server is in another network, because **the amount of data to move may be huge** (tens or hundreds of GB, especially for analyzed source files stored in `/srv/fossology`) and **may require days to complete - but in the meantime, the audit team needs to continue using Fossology**.

So it would not be viable to just freeze the old server, wait for the backup and restore process to complete in the new server, and then start using the new server, because it would stop the audit team work for too long.

Luckily Fossology backups may be performed "live" and incrementally (they use rsync over ssh), so **a good strategy could be to start a first "dirty" backup and restore process without stopping any service** on the old machine to get most of the data migrated. Then, a **"Migration Day"** should be set with the Audit Team for the "real" migration to happen. On such date, **the old server will be freezed**, but **the "clean" backup and restore process should take only a few hours**, and after that the Audit Team should be able to work on the new server.

So the whole process might be as follows:

a) installation and testing of the new Fossology server with dummy data by the sysadmin, who in this way may also start getting acquainted with Fossology (section 2 below)

b) setting up and testing of the VPN access to the new Fossology server (users may perform tests with dummy data, too, to check that everything works; see section 2.3)

c) first "dirty" backup and restore process, without stopping any service on the old machine, with the Audit Team keeping working on the old machine (sections 3.1 and 3.2); such step may be repeated multiple times in order to test the procedure and solve possible issues and/or do some optimization;

d) "Migration Day": `apache2` and `fossology` services are stopped in the old machine, a "clean" backup and restore process is performed, and then the Audit Team should be able to start working on the new server, hopefully after just a few hours stop (section 3.3.).

Steps a) and b) above may be partially performed in parallel with step c) (at least for the backup part; the restore process needs to be performed after a) and b) are completed).

## 2. Fossology deployment and testing (v4.1.0)

### 2.1. Installation

#### 2.1.1. Prerequisites

| Summary                                                                |
| ---------------------------------------------------------------------- |
| 16 cores, 32GB RAM, 500GB SSD storage, daily backups                   |
| Debian 10 minimal server (base system and ssh)                         |
| private network, VPN access for Fossology users                        |
| no unattended system upgrades, reboots or other maintenance operations |

##### a) do not use docker images, but a dedicated VM with at least 16 cores, 32GB RAM, 500GB SSD storage, daily backups

Although there is an official **docker image** available, the latter usually suffers from **various issues** at times, and in any case it is **not properly configured and tweaked** as suggested in the [official documentation](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning), so it may suffer from **performance issues** and even **hang in case of high workload**.

Last but not least, from our past experience integration with other tools may sometimes lead to **unforeseen issues** that may require the **restart of single services** or to **live patch/tweak Fossology's code/configuration** - which prove difficult when using the docker image.

From our experience, **installation from sources** on a **dedicated Debian machine** with **adequate resources** (at least **16 cores, 32GB RAM, 500GB SSD storage**) and with the **correct system configuration** ensures **more stability and reliability**.

As a footnote, a non-optimized Fossology installation may even work well for relatively small source components and for limited workloads. On the contrary, when big source components are involved (like the linux kernel, that has more than 70k files) and heavy workloads are expected (like with an operating system distribution, where hundreds or thousands of components are involved and are periodically updated and rewiewed in parallel by many auditors), if kernel, postgresql and php configurations are not optimized, Fossology may become unstable or even unusable.

As to backups, Fossology provides a basic backup and restore script (that needs manual customization) that may be used to perform regular backups of the database and of the scanned source files. It just requires a generic linux server with ssh and rsync, and an adequate storage, preferably in another geographic location.

Alternatively, **VM backup/snapshot facilities already available in the existing infrastructure may be used**. Such solution might be easier depending on the context, while the backup and restore script may still be used when data needs to be migrated from one server to another.

##### b) start from a Debian 10 minimal server installation (only base system and ssh, no other services, no other applications)

Even if Fossology officially supports various Debian and Ubuntu versions, the right pick is currently **Debian 10**.

Being very demanding in terms of system resources (at least in our use case), Fossology should be installed on a dedicated machine with adequate resources. For the same reason, Fossology must be installed on top of a Debian **minimal server installation** (**just base system and ssh service**), with no desktop environment and no other applications or services running on the same machine.

##### c) installation in a private network with VPN access for Fossology users

Having been designed from the very beginning as an internal application, Fossology never went through a security audit and **it is not safe to expose it to the Internet**: it should be kept in a **private network**.

Since Fossology users usually need to work from remote locations, a **VPN access** need to be properly configured for them.

##### d) no unattended system upgrades and reboots

Since on the one hand auditors may remotely work from different time zones and experience workload peaks, and on the other hand CI pipelines that upload source components to Fossology may be triggered at any time and require a long time to complete, **ideally Fossology should be available 24/7**.

Moreover, **unattended system upgrades may make Fossology scheduler crash** (typically, when postgresql packages are upgraded -- this would need a fix upstream, but in any case at least a restart of the scheduler would be required, that may still affect job execution).

So system **upgrades and reboots need to be agreed upon in advance** with the audit team and coordinated with **CI pipeline execution**, and should be generally avoided during audit workload peaks (tipically, near to a release deadline).

The same holds for Fossology maintenance operations (see section 2.2.g below).

#### 2.1.2. Quick installation script

(as root)

```bash
wget https://git.ostc-eu.org/oss-compliance/docs/-/raw/master/deployment/fossology/installation/install_fossology_on_debian_10.sh
chmod +x install_fossology_on_debian_10.sh
FOSSY_ENABLE_PHPPGADMIN=1 ./install_fossology_on_debian_10.sh
```

There is an [**official wiki**](https://github.com/fossology/fossology/wiki/Install-from-Source) to install Fossology from sources, but **currently is not complete** (Scancode installation fails because `pip` needs to be updated) and there are also some [**system configuration tweaks**](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning) to be implemented after installation.

Last but not least, **Fossology 4.1.0 Rest API still suffers from an [issue](https://github.com/fossology/fossology/issues/1800#issuecomment-712919785)** that leads to incorrect job status reports when a previous job had been killed (affecting in turn the integration with **Oniro compliance toolchain**, that needs to wait for the completion of Fossology jobs, and **may wait forever when the issue appears**). The Fossology development team aims at fixing it by an API refactoring, which is being implemented in a [currently open pull request](https://github.com/fossology/fossology/pull/1955): in the meantime **we temporarily patched the API**, in order to allow seamless integration with Oniro compliance pipelines.

To ease sysadmin work, we prepared a **simple script that automates all the necessary configurations and patches**.

Text notices and comments in the script already explain the various installation and configuration steps, so we will not go into the same details here.

### 2.2. Testing

The following is a **manual test procedure** to check if Fossology WebUI, scheduler and single scanners/agents have been installed and tweaked/patched correctly. The same tests might be automatically performed through Rest API, but **we want also to test the WebUI**; moreover, a manual test may also be a way, for sysadmins, to **get a first glance on how Fossology works** in the real world practice of a project like Oniro.

If you have a Fossology backup to restore, please note that all data created during the test will be overwritten by the backup restore procedure described later on.

If anything goes wrong or does not work as shown here, it means that something's wrong with the installation process; in such case, check if the prerequisites are actually met and try to re-install everything from scratch on a clean machine.

#### a) Change password

Login at `https://<ip-address>/repo` with user `fossy` and password `fossy` and then change the default user's password by selecting `Admin->Users->Edit User Account` from the top bar menu, and by inserting the new password here:

![](img/password.png)

Please note that **user's email address**, even if it is written that it "may be blank", **must contain at least one character**, otherwise some things may not work properly.

If you have a fossology backup to restore, keep in mind that any password you choose here will be overwritten by the backup restore procedure described later on.

#### b) Upload a package and run scanners/agents

Zlib is a typical test case for Fossology.
Download zlib-1.2.11.tar.gz from its [official website](https://zlib.net/fossils/zlib-1.2.11.tar.gz) and save it to your client PC.

Select `Upload->From File` from the top bar menu, and upload Zlib with the following options (please note that "Software Heritage Analysis" is unflagged: we do not use it and it is a very slow job since it uses an external API of a public service):

![](img/upload.png)

After clicking on "Upload", you should see the following notice at the top:

![](img/uploaded.png)

Click on "upload \#2", you should see a table indicating job progress statuses. When all jobs are completed, all lines should become green, like here:

![](img/jobs.png)

As a general remark, please note that every time you need to monitor the status of your jobs, you may select `Jobs->My Recent Jobs` from the top bar menu.


#### c) Download an SPDX report

On the top menu, choose "Browse", click on the dropdown menu near the uploaded package name, and choose "Export SPDX tag:value report"

![](img/download-spdx.png)

Save the file locally wherever you want.

#### d) Upload an SPDX report

Check the status of the uploaded package by clicking on "Browse" in the top bar menu, and then on the upload name "zlib-1.2.11.tar.gz". You should see all red or grey dots and no green dots, and in the right table, the "Concluded License Count" column should show only zeroes, like in this screenshot:

![](img/unclean-zlib.png)

Then, in the top bar menu, select "Upload -> Import Report" and upload the file you find here in `test/zlib_1.2.11-spdx.rdf`, with the following options:

![](img/upload-spdx.png)

You should be redirected to the jobs page. When reportImport agent is done, go back to the upload browse page. Now you should see a lot of green dots, and some non-zero numbers in the "Concluded License Count" column, like in this screenshot:

![](img/clean-zlib.png)

#### e) Test reuser agent

Choose `Organize->Uploads->Edit Properties` and rename the upload `zlib-1.2.11.tar.gz` to `zlib-1.2.11_OLD.tar.gz`. Then re-upload the zlib package by following the procedure described in section b) above. The scanners should take less time than before, because findings for already scanned files (based on their checksum) are automatically reused, if the scanner version is the same as before.

Check the status of the last upload as described in section d), first paragraph (all dots should be red or grey).

Then choose `Jobs->Schedule Agents` from the top bar menu, and schedule the reuser agent as shown here:

![](img/reuser.png)

Then, in the notification that should appear at the top, click on "View Jobs". When the reuser job has completed, go back to the upload browse page. You should see a lot of green dots as in the previous SPDX upload test described in section d).

#### f) Stress test: upload and scanning of big source packages and upload of big SPDX reports

This test is needed to check if patches and system configuration tweaks have been properly applied in order to allow Fossology to handle huge uploads and SPDX files.

In your local PC, download [lts-v5.10.1-yocto-201228T035809Z.zip](https://github.com/intel/linux-intel-lts/archive/refs/tags/lts-v5.10.1-yocto-201228T035809Z.zip) (it is a linux kernel fork snapshot used in Yocto). Then select `Upload->From File` from the top bar menu, and upload the source archive you just downloaded with the following options (we skip some analyses here; the most important thing is to avoid using Scancode on such huge uploads; its current integration in Fossology is not optimized and it could take forever to complete):

![](img/upload-linux.png)

Click on the link "Upload \#4" that appears in the notification at the top of the page to see the progress.

The whole process may take a long time (~2,5 hours), please be patient.

When it is done (all jobs turn green in the job table), open the upload by selecting "Browse" in the top bar menu and by clicking on its name (it may take some time to open it, since it is big). You should see red, grey and green dots. This is expected, since we run the "Ojo" license decider (that automatically applies Ojo findings -- i.e. [REUSE SPDX tags](https://reuse.software/tutorial/) -- as audit decisions if they do not contradict other scanners' findings).

Now let us try to upload a huge SPDX report file coming from another Fossology instance, where an audit team has already completed the audit work.

Locally unpack the test SPDX file found in this repo with

```bash
gunzip test/linux-intel_5.10.1_b8dd2066fa-spdx.rdf.gz
```

(uncompressed, it should be ~150MB)

select `Upload->Import Report` from the top menu, and upload the unpacked file with the following options (please note that you also need to select the lts-v5.10-yocto upload in "Select the upload you wish to edit"):

![](img/big-reportimport.png)

This will take a very long time, both to upload the file (you will see no progress indicator, but the browser's tab label should show that something is going on), and then to process it (do not worry if the progress indicator in the job status line remains stuck to "O items" for some time, it is normal; if you are worried anyway, you can check that things are going on by using `top` command from the terminal of the Fossology VM: you should see both `reportImport` and `postgresql` working a lot).

The whole process may take more than 1 hour to complete. When the line in the job table becomes green, it means that your Fossology machine passed the stress test!

#### g) Run maintenance operations

As a final test, we will run maintenance agent. Go to `Admin->Maintenance`, select "Run all maintenance operations", and then click on "Queue the maintenance agent"

![](img/maintenance.png)

As explained in the list above, there are some maintenance activities that need to be executed only when the system is not in use. For this reason, just like system upgrades and machine reboots also maintenance operations should be agreed upon with the maintenance team, based also on the system workload (number and size of source components uploaded, activity of the audit team) which may require to run them more or less frequently.

### 2.3. Testing VPN access by real users

After setting up the VPN access (we do not cover this topic here; any VPN access that seamlessly work on different client platforms, including linux, may work), real users should be able to test both their VPN access and the "new" Fossology instance with dummy data. All such data will be deleted by the subsequent migration procedure.

Dummy Fossology users should be created to perform this test (they will be deleted by the restore procedure described below).

Select `Admin->Users->Add` from the menu, and then fill in user data (remember to set Full Administrator access):

![](img/adduser.png)

Then you need to add such user to the `fossy` group. Select `Admin->Groups->Manage Group Users`, select the group `fossy` in the dropdown menu at the top, and then user `dummy` in the dropdown menu in the first column of the table, and then select `Admin` in the second column (you do not need to click on a confirmation button, all changes are immediately applied here).

![](img/group.png)

Create as many dummy users as needed by the Audit Team to perform access tests.

## 3. Backup and restore (data migration)

### 3.1. First "dirty" backup of the old Fossology instance

> this step may be performed in parallel with the previous steps described in sections 2 and 3 above - i.e. installation and testing of the new instance

Create a temporary backup VM (preferably in the old infrastructure) with 500GB storage, Debian 10 minimal server (only base system and ssh) **exposed to the Internet**. Add just base security and create the backup user:

(as root)

```bash
apt update
apt install ufw fail2ban
ufw allow ssh
ufw enable
adduser fossy
su - fossy
mkdir ~/backup
exit
```

Then, on the "old" Fossology machine (as root):

```bash
su - fossy
test -f ~/.ssh/id_rsa.pub || ssh-keygen  # leave defaults if asked
ssh-copy-id fossy@<backup-vm-ip-or-hostname> # accept host key
# (alternatively, if you want to disable ssh password
# authentication, you should manually copy the ssh key of the
# Fossology machine in the backup machine)
ssh fossy@<backup-vm-ip-or-hostname> # test connection
exit # exit from the test connection
exit # return to root user
ssh -i /home/fossy/.ssh/id_rsa fossy@<backup-vm-ip-or-hostname> # test connection and accept host key also for root user (required...)
exit # exit from the test connection
cd /fossology/utils/backup
nano fo-backup
```

change the following lines:

```bash
# user variables
BACKUPDIR=/home/fossy/backup               # Directory on the backup server to store backup data
KEY=/home/fossy/.ssh/id_rsa             # ssh PRIVATE key about backup user 'fossy'
SOURCEDIR=/srv/fossology/                  # Source directory to backup
PGBACKUPDIR=/srv/fossology/db              # Directory to store database backup data
BACKUPUSER=fossy@<backup-vm-ip-or-hostname>            # Backup user and hostname/ip
EXCLUDES=(./backup/backup_include_a ./backup_include_b)     # rsync "exclude" files (see man rsync, --exclude-from option) in order to set which backup directories and files are to include or exclude
```



Save and exit, then:

```bash
nohup ./fo-backup -v &
```

To monitor the progress:

```bash
cd /fossology/utils/backup
tail -f nohup.out
```

Depending on the data size and on the network, this backup may take up to 1-2 days to complete. During the backup process, the Audit team should be able to continue working on the "old" machine.

### 3.2. First "dirty" restore on the "new" Fossology server.

On the "new" Fossology machine, installed by following section 2 above

(as root):

```bash
su - fossy
test -f ~/.ssh/id_rsa.pub || ssh-keygen # leave defaults if asked
ssh-copy-id fossy@<backup-vm-ip-or-hostname> # accept host key
# (alternatively, if you want to disable ssh password
# authentication, you should manually copy the ssh key of the
# Fossology machine in the backup machine)
ssh fossy@<backup-vm-ip-or-hostname> # test connection
exit # exit from the test connection
exit # return to root user
ssh -i /home/fossy/.ssh/id_rsa fossy@<backup-vm-ip-or-hostname> # test connection and accept host key also for root user (required...)
exit # exit from the test connection
cd /fossology/utils/backup
nano fo-restore
```

change the following lines:


```bash
# user variables
BACKUPDIR=/home/fossy/backup               # Directory on the backup server to store backup data
KEY=/home/fossy/.ssh/id_rsa             # ssh PRIVATE key about backup user 'fossy'
SOURCEDIR=/srv/fossology/                  # Source directory
PGBACKUPDIR=/srv/fossology/db              # Directory to store database backup data
BACKUPUSER=fossy@<backup-vm-ip-or-hostname>            # Backup user and hostname/ip
EXCLUDES=(./backup_include_a ./backup_include_b)    # rsync "exclude" files (see man rsync, --exclude-from option) in order to set which backup directories and files are to include or exclude
TEMPDIR=/tmp
RESTOREDIR=current                         # sub directory under $BACKUPDIR to restore from
```

Save and exit, then:

```bash
nohup ./fo-restore -v &
```

To monitor the progress:

```bash
cd /fossology/utils/backup
tail -f nohup.out
```

If, in the "old" machine, **Fossology's default db password ("fossy") was changed**, that change should reflect also in Fossology's configuration files in the "new" machine (otherwise you may get an "Unable to connect to fossology database" error when trying to open to the WebUI). In such case, copy the `password` value found in `/usr/local/etc/fossology/Db.conf` in the "old" machine, and paste it in the same location in the "new" machine. Then restart the fossology service in the "new" machine with `service fossology restart`.


### 3.3. "Migration Day": clean backup and restore

> Before the Migration Day, please make sure that Fossology users are able to access the new Fossology server through VPN

On the old Fossology machine (as root)

```bash
service apache2 stop # stop WebUI so no one can work on Fossology
# fossology scheduler service will be stopped by the backup script
cd /fossology/utils/backup
nohup ./fo-backup -v &
tail -f nohup.out
```

When it's done (expected time: ~2 hours), on the new Fossology machine (as root)

```bash
service apache2 stop # stop WebUI so no one can work on Fossology
# fossology scheduler service will be stopped by the restore script
cd /fossology/utils/backup
nohup ./fo-restore -v &
tail -f nohup.out
```

When it's done (expected time: ~2 hours):

```bash
service apache2 start
```

Now users should be able to work on the new Fossology server.
