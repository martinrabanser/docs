---
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: 2020-2022 Alberto Pianon <pianon@array.eu>
---

# Fossology Installation Script

Simple script to install Fossology 4.1.0 from sources in Debian 10 (Buster), and adjust server settings according to Fossology's official documentation at https://github.com/fossology/fossology/wiki/Configuration-and-Tuning.

Settings are tweaked and some code is patched in order to fix a known bug in rest API, in order to get correct job status and avoid issues in compliance pipeline execution (see the comments in the script for more details).

After installation, you can access Fossology at `https://<fossology_server_address>/repo`.

Default user/password both for Fossology and for the Postgres DB are fossy/fossy.

You may want to change default password both in Fossology and in Postgres DB. In case you change postgres DB password, you have to put the new DB password in `/usr/local/etc/fossology/Db.conf`, otherwise Fossology will not work.

## Enabing phppgadmin

Phppgadmin may be useful in order to allow easier inspection of Fossology's database in case of issues, need of manual fixes etc.
If you want to enable it, you should do

```bash
FOSSY_ENABLE_PHPPGADMIN=1 ./install_fossology_on_debian_10.sh
```

## Testing other fossology versions

By default, this script installs Fossology 4.1.0 and it has not been tested with other versions. If you want to try to install another version only for testing purposes, you may force it by doing:

```bash
FOSSY_RELEASE=<version_number> ./install_fossology_on_debian_10.sh
```
