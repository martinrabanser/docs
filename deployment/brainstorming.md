---
title: Oniro Compliance Toolchain - Getting started guide
author: Peter Moser
date: 2022-07-19
lastmod: 2022-07-19
draft: true
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Brainstorming

These are just ideas, maybe feasible, maybe EF has a complete other approach.
Just listing everything that comes in mind for further discussions...

## CI / CD consideration

- Automatic Continuous Integration (CI) on each push or merge request
- Automatic Continuous Deployment (CD) on each push to the branch `main` on a
  staging environment
- Automatic Continuous Deployment (CD) on each push to the branch `prod` on a
  production environment
- Rename all `master` branches to `main` and consider them for staging/testing
  only, use it as the default branch
- Add another branch, called `prod` to deploy on production
- Versions of tools and docker images should have the gitsha as version to be
  able to trace back possible errors from a running instance to source code
- NOI Techpark uses [Ansible] to deploy to servers, we have already some scripts
  for that if EF likes it (please note, I talk just about the Ansible tool, not
  any SaaS or platform)...

## Documentation and README.md files

**TODO**: Update all links and remove all deprecated sections that point to old
NOI or OSTC links, that are no longer valid after full migration. This needs to
be done, during and after the migration to EF repos and servers.


[Ansible]: https://docs.ansible.com/ansible/latest/user_guide/index.html#getting-started
