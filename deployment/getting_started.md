---
title: Oniro Compliance Toolchain - Getting started guide
author: Peter Moser
date: 2022-07-19
lastmod: 2022-07-19
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Oniro Compliance Toolchain - Getting started guide

This is a short introduction for new contributors.

First, let's give you a short introduction of this project. What it is and what
we have done so far. Secondly, lets draw a little picture of all services and
servers, repos and other material we have. Happy hacking :-)

## Introduction

Our main goal is to implement a Continuous Compliance workflow for
[Oniro]-related repositories. The idea is to continuously scan (through license
scanning tools, like [ScanCode] and [Fossology]) the source code of all first
party and third party components that are included in Oniro reference builds for
multiple possible targets, so that the scan results can be continuously
validated by the Audit Team, and potential Intellectual Property (IP) issues can
be spotted and solved early in the development process.

It must be said, that this is a hard task, due to the nature of license and
copyright information present in todays source code repositories, where a
package manager is missing, and where source code provenance and
license/copyright metadata are often missing, messy, uncertain and/or imprecise.

To accomplish this task in this demanding context, we implemented a Python tool
for **Software Composition Analysis (SCA)** called `Aliens4Friends`, that
continuously analyzes Yocto builds and tries to automatically detect as many
license and copyright information as possible, by comparing "alien" source
packages with packages found in existing trusted sources, like for instance
Debian. We took Debian as a primary "trusted source" because it has a strict
policy to include packages in its distribution (also from a FLOSS compliance
standpoint) and because it is backed by a community that continuously checks and
"audits" source code to that purpose. Other similar sources of truth may be
added in the future. The overall idea is to avoid reinventing the wheel: if
copyright and license metadata have already been reviewed by a trusted
community, it does not make sense to redo their work by auditing the same code
back to square one. More generally, the idea is that if a similar (or same)
software package has been already included in Debian, it means that it is a
well-known component, so it is a presumed friend, and we can safely invite it to
our party.

**We have also a video that explains the [WHY AND WHAT] of Aliens4Friends. Enjoy
:-)**

Another gear in our project is Gitlab, which triggers the execution of our SCA
pipeline if the Oniro project contributors push to the main repo, which contains
a so-called manifest, that contains the recipe to build all necessary
combinations of the project with different hardware boards, images, package
versions, and operating systems.

The workflow of our pipeline is as follows: ![Aliens4Friends Workflow Overview]

**NEXT STEPS**:
- Refer to the [IP Compliance Pipeline] and the [Aliens4Friends] documentation,
if you need more details.
- We have [some videos and slides](../demo/history/202106-toolchain_demo), which
  refer to an older (now partly outdated) version of our pipeline, which could
  nevertheless give you some good overview of what we do and why we did it like
  this.

## The project landscape

Here we talk about services, servers and repos.

### Source Code Repos

Currently we have these repositories at **OSTC**:
- [Tinfoil Hat + Aliensrc Creator](https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat) \
  A wrapper around Yocto's tinfoil command, to extract licensing and copyright
  information of a concluded build, and a tool to create package archives with
  associated metadata of used packages in that build.
- [Aliens4Friends](https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends) \
  The main project repo. Read the [Introduction](#introduction) to understand
  what it is. We also have a [Contributor'S
  FAQ](https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends#contributors-faq).
  Please read that first if you like to improve it.
- [OSS Compliance GitLab Pipeline](https://git.ostc-eu.org/oss-compliance/pipelines) \
  The GitLab pipeline yml files, that run Aliens4Friends, create artifacts for
  badges and reports.

At **NOI** we work with https://github.com/noi-techpark. [All repos related to
this project](https://github.com/noi-techpark?q=solda&type=source) contain
`solda`.

- [project-solda](https://github.com/noi-techpark/project-solda) \
  contains the general overview, related issues and epics. We mostly just link
  these user stories to the original ones at https://git.ostc-eu.org
- [solda-debian2spdx-standalone](https://github.com/noi-techpark/solda-debian2spdx-standalone) \
  was a first attempt to separate this Aliens4Friend's subcommand and put it
  into a standalone command. It is not complete yet, low priority.
- [solda-aliens4friends-dashboard](https://github.com/noi-techpark/solda-aliens4friends-dashboard) \
  The Dashboard is a webapp that takes the `harvest.json` files from the last
  step of an Aliens4Friends pipeline and shows it in a nice dashboard with
  filters and statistical representations.
- [solda-aliens4friends-api](https://github.com/noi-techpark/solda-aliens4friends-api) \
  Just a very limited PoC to create an API out of the current Postgresql DB,
  that contains Aliens4Friends pool data. At the moment only tinfoilhat related
  things. Not used at the moment, likely a future requirement.
- [solda-aliens4friends](https://github.com/noi-techpark/solda-aliens4friends) \
  This is just a read-only mirror of
  https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends, please do not
  push to it. It was a test on Github to see how mirroring works. We might even
  close it, to not confuse it with the real source code repo.

As you see we have different repos on various platforms, that all need migration
to the new Eclipse Foundation (EF) GitLab instance at https://gitlab.eclipse.org

In addition there are some more links at **OSTC** that contain demos,
installation scripts/instructions, epics/user stories, or general documentation:
- https://git.ostc-eu.org/oss-compliance
- https://git.ostc-eu.org/groups/oss-compliance/-/issues
- https://git.ostc-eu.org/dashboard/todos

In an intermediate step of migration we also had
https://booting.oniroproject.org/, which will not be used, once we finally
migrate to EF infrastructure, at least not for our SCA project.


### Servers

We have only two servers at the moment:
- Fossology (managed by OSTC)
- GitLab Runner's host machine (managed by OSTC)

#### Fossology

It should not be necessary that you access the webapp, server or Postgres of the
production Fossology. If so, please ask Alberto for help...

#### Gitlab Runner's host machine
To access this machine via SSH, head over to OSTC's Mattermost and ask for a
[tailscale](https://tailscale.com/) access to that server. Tailscale is a VPN
over which you can then run ssh.

Here are two convenience configurations for `~/.ssh/config`. Just replace
`yourlogin` and `1.2.3.4`:
```
Host soldaprod-novpn
    User yourlogin
    Hostname 1.2.3.4
Host soldaprod
    User yourlogin
    Hostname 1.2.3.4
    LocalCommand bash -c 'echo STARTING TAILSCALE; sudo tailscale up'
```

Then you can call `ssh soldaprod` which will automatically start tailscale if it
is not yet running, eventually asking for your `sudo` password. If you are sure
that it is already running, just call `ssh soldaprod-novpn`

### Services

#### SCA dashboard
The Dashboard runs on `Gitlab Runner's host machine`. It is accessible at https://sca.ostc-eu.org/.

If you want to update it, do the following:
- `ssh soldaprod`
- `sudo su`
- `cd /var/docker/dashboard/git`
- `git pull`
- `cd ../current`
- `docker-compose stop; docker-compose up --build -d`
- Eventually check startup logs with `docker-compose logs -f`

If you need to install it on a new machine, follow these steps:

1) Install Docker and docker-compose.
2) Clone the source code
	```sh
	mkdir -p /var/docker/dashboard/current
	cd /var/docker/dashboard
	git clone https://github.com/noi-techpark/solda-aliens4friends-dashboard.git
	```
3) Inside `/var/docker/dashboard/current`, create a `docker-compose.yml` file as follows:

```yml
version: "3.4"
services:
app:
  image: solda-dashboard:prod
  restart: unless-stopped
  build:
  context: ../git/
  dockerfile: infrastructure/docker/Dockerfile
  target: build
  env_file:
  - .env
  ports:
  - ${SERVER_PORT}:80
```
4) Create a `.env` file as follows, please change parameters eventually:
	```ini
	APP_ENV=production
	APP_DEBUG=false
	APP_URL=https://sca.ostc-eu.org/
	SERVER_PORT=1077
	```
5) Finally, run `docker-compose up -d`. You can watch startup logs with `docker-compose logs -f`

**IMPORTANT**: The SSL certificates and https are handled outside, managed by
OSTC. At the moment their proxy just points to port 1077 of this machine. In the
future automatic CI/CD pipelines would be better. This is just an intermediate
manual deployment, not suited for a production environment later on.

#### Aliens4Friends API

This is just a [PostgREST installation](https://postgrest.org/) installed on
`Gitlab Runner's host machine` and currently accessible at https://api.solda.testingmachine.eu/.
If you like to see a swagger interface use https://swagger.opendatahub.com/?url=https://api.solda.testingmachine.eu.

Since this is just a very minimal PoC, use it as is.

1) Current installation instruction:
	```sh
	ssh soldaprod
	sudo su
	mkdir -p /var/docker/api
	```

2) Create a `docker-compose.yml` file with the following content:

```yml
version: '3'
services:
  server:
  	image: postgrest/postgrest
  	ports:
  	- "3000:3000"
  	environment:
  	PGRST_DB_URI: postgres://solda_api_admin:solda_api_admin_password@test-pg-bdp.co90ybcr8iim.eu-west-1.rds.amazonaws.com:5432/solda
  	PGRST_DB_SCHEMA: public
  	PGRST_DB_ANON_ROLE: solda_api_admin #In production this role should not be the same as the one used for the connection
  	PGRST_OPENAPI_SERVER_PROXY_URI: http://127.0.0.1:3000
```

3) As you see the Postgresql DB is on NOI's AWS/RDS and called
   `test-pg-bdp.co90ybcr8iim.eu-west-1.rds.amazonaws.com`, the DB is `solda`.
   Please replace the `solda_api_admin_password` with the real password. The
   password can be found in passbolt.

4) Finally, run `docker-compose up -d`. You can watch startup logs with `docker-compose logs -f`


#### Gitlab Runner installation and Gitlab setup

See https://git.ostc-eu.org/oss-compliance/pipelines#gitlab-setup

#### Gitlab at OSTC

Go to https://git.ostc-eu.org and sign up there.

#### Mattermost at OSTC

To get in contact with OSTC use their Mattermost instance:
https://chat.ostc-eu.org/. Here you can ask for help, and access grants to
servers if necessary.

You need to login to Mattermost with your Gitlab account, so create one before.

#### Gitlab at Eclipse Foundation

See https://gitlab.eclipse.org. Open an account at eclipse.org and then ask project maintainers to get access.



<!-- LIST OF LINKS (INVISIBLE) -->
[WHY AND WHAT]: ../demo/history/202106-toolchain_demo/video/a4f_intro_why_what.mp4

[IP Compliance Pipeline]: https://git.ostc-eu.org/oss-compliance/pipelines#ip-compliance-pipelines

[Aliens4Friends]: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends#aliens-for-friends

[Oniro]: https://oniroproject.org/
[ScanCode]: https://github.com/nexB/scancode-toolkit
[Fossology]: https://www.fossology.org/

[Aliens4Friends Workflow Overview]: https://git.ostc-eu.org/oss-compliance/pipelines/-/raw/master/img/aliens4friends-schema-2021-12-17.png
