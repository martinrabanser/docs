---
title: Oniro Compliance Toolchain - Getting started guide
author: Peter Moser
date: 2022-07-19
lastmod: 2022-07-20
draft: true
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Oniro Compliance Toolchain - Migration Plans

*If you are not familiar with this project, please refer to the [Getting Started
Guide](getting-started-guide.md) first.*

Everything about the migration from OSTC / NOI to EF infrastructure

Lets shortly describe what we need to do for a full migration from OSTC and NOI
repos, servers and services to the EF infrastructure. For existing documentation
please just link to that, so we do not duplicate entries, that might be
out-of-sync at some point in time.

**Table of Contents**
- [Migration Plans](#migration-plans)
	- [Repositories](#repositories)
		- [Originals](#originals)
		- [On EF GitLab](#on-ef-gitlab)
	- [GitLab instance](#gitlab-instance)
		- [License](#license)
		- [Oniro Core Mirroring](#oniro-core-mirroring)
		- [Docker Registry](#docker-registry)
	- [GitLab Runner](#gitlab-runner)
	- [SCA Dashboard](#sca-dashboard)
	- [Fossology](#fossology)
	- [Maintenance](#maintenance)
		- [Updates](#updates)
		- [Backups](#backups)
		- [Toolchain maintenance (developer's duty)](#toolchain-maintenance-developers-duty)
	- [Brainstorming](#brainstorming)

## Repositories

### Originals
- Tinfoil Hat: <https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat>
- Aliens4Friends: <https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends>
- OSS Compliance GitLab Pipeline: <https://git.ostc-eu.org/oss-compliance/pipelines>
- SCA Dashboard: <https://github.com/noi-techpark/solda-aliens4friends-dashboard>
- Aliens4Friends API: <https://github.com/noi-techpark/solda-aliens4friends-api> \
  (not used at the moment, likely a future requirement)

### On EF GitLab
This is just a proposal, let me know what you think...

Starting from the project https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain.

**QUESTIONS**:
1) Would it be possible to rename it from `oniro-compliancetoolchain` to
   `oniro-compliance`?
2) Is it possible to keep the git history, or do we need a squashed first import
   as a single commit?

Repositories inside that project (with sub-folders):
- Tinfoil Hat: `toolchain/tinfoilhat`
- Aliens4Friends: `toolchain/aliens4friends`
- OSS Compliance GitLab Pipeline: `pipelines`
- SCA Dashboard: `toolchain/aliens4friends-dashboard`
- Aliens4Friends API: `toolchain/aliens4friends-api`
- General documentation: `documentation` for overviews
- Mirrors: Create a `mirrors` subfolder, and inside that all needed
  repositories... see chapter [Oniro Core Mirroring](#oniro-core-mirroring) for
  details...

## GitLab instance

### License
At the moment we need to have an EE licensed GitLab instance to allow `Run CI/CD
for external repository`. With that we can mirror all projects that a subject to
Software Composition Analysis (SCA).

### Oniro Core Mirroring

At the moment to run OSS Compliance Pipelines we need to mirror certain
repositories, for example the `Oniro Goofy` repo:
https://gitlab.com/noi-techpark-premium/solda/mirrors/oniro-goofy

For that we need to:
1) Create a mirrors inside:
   https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/mirrors/
2) Define [badges in GitLab](https://gitlab.com/help/user/project/badges)

Example: https://gitlab.com/noi-techpark-premium/solda/mirrors/oniro/edit --> See Badges...

### Docker Registry

The Aliens4Friends tool creates two docker images and pushes them to the
repository's own [Container Registry]. These images are then used in the
pipelines.

As for now, we only need a "Container Registry" and write access to it, to push
and use those images later on.

The GitLab CI file [.gitlab-ci.yml] shows how we push new images, and
[p1-default.yml#L290] is an example of how we use it.


## GitLab Runner

The GitLab Runner should be dedicated to the OSS Compliance scans only and have
the following configuration:

- [Server hardware and software requirements]
- [General installation instructions]
- [GitLab runner configuration]

**INFO**: In this document we talk about NOI Techpark Gitlab account, which is
no longer needed after migration. If the EF Gitlab server has premium features,
we can simply switch to that instance without any need of the of mirrors on
<https://gitlab.com/noi-techpark-premium> or
<https://git.ostc-eu.org/oss-compliance>.

**TODO**: Once we migrate <https://git.ostc-eu.org/oss-compliance/pipelines> to
an EF repo, we should update that chapter accordingly.

**QUESTIONS**:
1) The current docker configuration on the GitLab runner allows a caller to
   directly use the docker daemon of that server. That is, the runners' docker
   is `privileged`. Is that OK, or should we use a docker-in-docker setup? The
   latter was never used, nor do we have any experience with that at the
   moment...
2) Sometimes it is useful to access the GitLab runner directly to debug pipeline
   runs, or to cleanup an eventually unforeseen state of the pipeline. Would it
   be possible to have a (limited) SSH access to that server?
3) Do we start with an empty Aliens4Friends pool and build directory, or do we
   need to import them from the old instance? They have several GB of data at
   the moment...

## SCA Dashboard

We need a domain to access this board. At the moment it is on
https://sca.ostc-eu.org. In the future it could be something like
https://sca.oniroproject.org, or maybe some subdomain on eclipse.org?

In addition we need a proxy (apache2 or caddy or ...) to handle ssl certificates
and do a http to https redirect, and forwarding to a certain port on the server
which hosts this application.

The host needs to have docker and docker-compose installed, and give access to
the proxy on the exposed port of the SCA Dashboard docker container.

Ideally, but optional, we would have also a test installation to see it in
staging environments on another domain. For example,
https://scatest.oniroproject.org or something similar.

We need to decide which domain we want to use for these tools.

## Fossology
See [fossology_migration.md] inside the `fossology-administration` repo.

## Maintenance

This project is in a work-in-progress state, relying on several external tools,
that are not meant to work with such huge data. In addition, this pipeline
monitors a project in development, which makes it very important to have an easy
and fast debug/development cycle for contributors. Therefore, it is crucial from
time to time to access resources for debugging and maintenance work. The
additional effort to manage these resources without access, is unproportianal
higher due to the uncertainty of a huge fast moving project like Oniro.

Here is a list of permission requests:
- **Fossology Database Access**: Sometimes Fossology creates database entries,
  that need manual fixes. For that to work, we installed `phppgadmin` to have a
  web app to access database tables and execute SQL queries, which also have
  `insert`, `delete`, `create`, `drop`, and `update` privileges.
- **Fossology Scheduler Restart**: Sometimes (in our experience approx. once a
  month) the scheduler backend job crashes or does not respond, which makes it
  necessary to get it restarted. For that we would need access to the server
  with minimal privileges to restart that process.
- **Aliens4Friends Pool and Build Dir Access**: The pool is a folder on the
  Gitlab Runner Host, which has all intermediate and final results of the
  Compliance Pipeline runs: for complex reports, debugging purposes, or error
  reproduction we would need to access it. There is also another permanent
  folder on the Gitlab Runner Host, used by Compliance Pipelines to fetch
  sources, build the whole target matrix and collect metadata: since sometimes
  builds fail and block the whole compliance process, we need to access it for
  debugging purposes.  This means we would need a SSH access to the Gitlab
  Runner Host with read and write privileges these two directories (`/a4fpool`
  and `/build`).

### Updates

- **Fossology Server + PostgreSQL DB** \
  It might be rarely needed to update Fossology, and its underlying Postgresql
  database. Ideally, we would then have a parallel instance of both services
  like we need it now for the first migration. So we could test new Fossology
  versions, but in the meantime be able to continue the auditing tasks. Finally,
  with a delta-script migrate to the new tested Fossology instance. Please note,
  that some Fossology updates might be problematic due to incompatibilities in
  their APIs which results in additional development effort for "Aliens4Friends"
  contributors (see an example [here](https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/issues/2)).
  This makes it necessary to study, evaluate and decide which Fossology version
  we can upgrade to and when and how we should do it.
- **VPN to access the Fossology webapp** \
  Since Fossology has never got a security check, it is not safe to have it
  exposed to the Internet. Nevertheless, we need to have access to it from all
  places. This VPN setup might need security updates, and configuration of user
  accounts.
- **Gitlab Runner Host** \
  Currently we run and test on Debian 10. We should regularly update that
  server, and in the future migrate to a newer Debian version.
- **Gitlab Runner executable** \
  If we eventually update the Gitlab instance, we might need to update the
  Gitlab Runner executable as well. This could also be necessary from time to
  time with the same Gitlab version, if new security fixes got released.
- **Gitlab Runner Host's Docker installation** \
  We need to install and maintain a Docker daemon and docker-compose
  installation inside the Gitlab Runner Server, because the pipelines use a
  privileged Gitlab Runner configuration, which accesses the host's docker daemon
- **Dashboard Webapp** \
  We need to have and maintain some place where we store the static dashboard
  webapp, certificates might need some extra work, http to https redirects
  needed.
- **Gitlab CI and Docker Registry** \
  Eventual Continuous Integration and Deployment pipelines that deploy staging
  and production versions of the dashboard and push toolchain docker images to
  the local docker registry. This should be a one-time setup, with very little
  maintenance costs afterwards. The registry might need backups. We do not have
  experiences of running Gitlab on our own servers, so that needs investigation
  to understand the maintenance effort.
- **Mirrorered Repos** \
  We need to mirror repos in the `mirror` project on Gitlab. These projects need
  to be configured with badges and an external CI Yaml script. At the moment
  only done once for each Oniro Release Cycle.

### Backups
- Fossology Data (Database and files: See [fossology_migration.md] inside the
  `fossology-administration` repo for more details)
- Aliens4Friends pool (files only)

### Toolchain maintenance (developer's duty)

Internal maintenance work, just if we change something inside the toolchain
docker images. This chapter might not be relevant for a sysadmin's maintenance
work, but to have a complete picture I put it here.

This is only relevant if we change the [toolchain.dockerfile] inside the
[aliens4friends] repo.

- Python (currently v3.8)
- Scancode (currently fixed version 3.2.3)
- We use various command line tools, which should not give any issues when
  upgrading. Their CLI is considered stable.
- Aliens4friends maintenance and updates needed, when we switch to another
  Yocto version or when we use a newer Scancode

If we change one or more dependencies listed above, we need testing and
additional coding on the Aliens4Friends repo and related projects.


## Brainstorming

This chapter is just a list of ideas, that came up during the writing of this
document. Might be interesting, might be ignored...

See [Brainstorming](brainstorming.md).


<!-- Aliases (hidden here) -->
[fossology_migration.md]: https://git.ostc-eu.org/oss-compliance/fossology/fossology-administration/-/blob/main/fossology_migration.md

[Container Registry]: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/container_registry

[aliens4friends]: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends

[toolchain.dockerfile]: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/-/blob/master/infrastructure/docker/toolchain.dockerfile


[.gitlab-ci.yml]: https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends/-/blob/master/.gitlab-ci.yml

[p1-default.yml#L290]: https://git.ostc-eu.org/oss-compliance/pipelines/-/blob/master/p1-default.yml#L290

[Server hardware and software requirements]: https://git.ostc-eu.org/oss-compliance/pipelines#server
[General installation instructions]: https://git.ostc-eu.org/oss-compliance/pipelines#gitlab-setup
[GitLab runner configuration]: https://git.ostc-eu.org/oss-compliance/pipelines#gitlab-runner-configuration
